Dev.
Table:
	cbrm_datamart_assigned_billing_offer_edo
	cbrm_datamart_assigned_product_parameter_edo
	cbrm_datamart_assignedproduct_edo
	insert_tbl_assignedproduct


Glue Job:
	dm-dv-assignedproduct - Updated Script
	dm-dv-assignedproductparameter
	dm-dv-assignedbilling
	dm-dv-assignedproduct-tbl
	adhoc_dm-dv-assignedproduct-tbl
 


1.The purpose of the 'dm-dv assigned billing, assignedproduct, and assignedproductparameter' glue jobs is to copy data from 's3-prd0edo-data-olyp' to the 's3cbrmdv02' S3 bucket.
2. The purpose of the 'dm-dv-assignedproduct-tbl' job is to insert data into the 'tbl_assignedproduct' table in Athena. The WHERE filter is MAX(prod.p_date). 
Note: This dm-dv-assignedproduct-tbl and adhoc_dm-dv-assignedproduct-tbl job does not exist in prod.


TODO in prod:
1. Apply the changes in 'dm-dv-assignedproduct' - Updated Script and rerun.
   - Note: Comment out this part: '#elif 'LastModified' in i and i['LastModified'].date() > date_yesterday.date(): #continue'
2. Create a directory called 'sql_script/assignedproduct' if it doesn't already exist and upload the 'insert_tbl_assignedproduct.sql' script.
3. Use the 'adhoc_dm-dv-assignedproduct-tbl' to run the previous date with the following WHERE filter: they have 'start_param' and 'end_param'.
4. For the 'dm-dv-assignedproduct-tbl', add a scheduled job to load data into the 'tbl_assignedproduct' in Athena.
