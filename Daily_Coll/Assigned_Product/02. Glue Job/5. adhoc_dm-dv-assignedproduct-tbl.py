import sys
import time
import pandas as pd
import boto3
import datetime

from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3cbrmddv02"
        self.DATABASE_NAME = "cbrm_prd_datamart"
        self.start_param = "2021-01-01"
        self.end_param = "2024-01-01"

    def run_athena_script(self):
        """Run SQL script to populate target table with the latest subid faid pair hist"""
        print("--- Run SQL script to populate target table")

        athena_client = boto3.client("athena")

        config = {
            "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
        }

        # Retrieve the SQL script from the S3 bucket
        bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
        obj = bucket.Object("sql_script/assignedproduct/insert_tbl_assignedproduct.sql")
        script = obj.get()["Body"].read().decode("utf-8")

        # Split the script into separate statements
        statements = script.split(";")

        # Define the context dictionary
        context = {"Database": self.DATABASE_NAME}

        for statement in statements:
            # Trim leading and trailing whitespaces
            statement = statement.strip()

            # Skip empty statements and comments
            if not statement or statement.startswith("--") or statement.startswith("/*"):
                continue

            # Delete SQL statement
            delete_sql = f"DELETE FROM tbl_assignedproduct WHERE product_p_date BETWEEN '{self.start_param}' AND '{self.end_param}';"
            print(delete_sql)

            # Start query execution for DELETE statement
            res_delete = athena_client.start_query_execution(
                QueryString=delete_sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )

            query_execution_id = res_delete["QueryExecutionId"]
            status = self.wait_for_query_completion(query_execution_id, athena_client)
            print(status)

            # Prepare the SQL statement
            prepare_sql = f"PREPARE insert_tbl_assignedproduct FROM INSERT INTO tbl_assignedproduct {statement} WHERE prod.p_date BETWEEN '{self.start_param}' AND '{self.end_param}';"
            print(prepare_sql)

            # Start query execution for PREPARE statement
            res_prepare = athena_client.start_query_execution(
                QueryString=prepare_sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )

            query_execution_id = res_prepare["QueryExecutionId"]
            status = self.wait_for_query_completion(query_execution_id, athena_client)
            print(status)

            # Execute the prepared statement
            execute_sql = f"EXECUTE insert_tbl_assignedproduct;"
            print(execute_sql)

            # Start query execution for EXECUTE statement
            res_execute = athena_client.start_query_execution(
                QueryString=execute_sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )

            query_execution_id = res_execute["QueryExecutionId"]
            status = self.wait_for_query_completion(query_execution_id, athena_client)
            print(status)

            # Retrieve the results of the EXECUTE statement
            results = athena_client.get_query_results(QueryExecutionId=query_execution_id)
            # Process and use the results as needed
            print(results)

    def wait_for_query_completion(self, query_execution_id, athena_client):
        """Wait for the query execution to complete"""
        while True:
            response = athena_client.get_query_execution(QueryExecutionId=query_execution_id)
            state = response['QueryExecution']['Status']['State']
            if state in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
                break
            time.sleep(5)

    def process(self):
        self.run_athena_script()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
