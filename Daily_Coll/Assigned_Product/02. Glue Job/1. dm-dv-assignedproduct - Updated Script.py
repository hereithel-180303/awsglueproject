import boto3
import pandas as pd
import pyarrow
import re
import sys
import io
from datetime import datetime, timedelta
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3-prd-edo-data-olyp"
        self.DATABASE_NAME = "cbrm_prd_datamart"
        self.NEW_BUCKET_NAME = "s3cbrmddv02"

    def csv_to_parquet(self):
        """convert csv files to parquet files"""
        print("--- Convert files from csv to parquet")
        regex_ymd = "\d{4}\d{2}"
        regex_filename = "\/(?:.(?!\/))+$"
        #prefix = "compute/opt_adh/data/com_globe_dh_shared_mutables_assignedproduct_assignedproductmutable"
        prefix = "compute/opt_adh/data/com_globe_dh_shared_mutables_assignedproduct_assignedproductmutable/p_date=2021-11-01"

        paginator = self.s3_client.get_paginator("list_objects_v2")
        pages = paginator.paginate(Bucket=self.BUCKET_NAME, Prefix=prefix)
        date_yesterday = datetime.now() - timedelta(days=1)
        for response in pages:
            if 'Contents' in response:
                print(response["Contents"])
                for i in response["Contents"]:
                    if i["Key"][::-1][0] == "/":
                        continue
                    ## Note: Comment the lastmodified condition during first run to convert all files
                    #elif 'LastModified' in i and i['LastModified'].date() > date_yesterday.date():
                    #    continue
                    elif 'p_state=published' not in i['Key']:
                        continue

                    obj_key = i["Key"]
                    print(obj_key)
                    copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
                    s3_bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
                    obj = s3_bucket.Object(key=obj_key)
                    print(obj)

                    filename = obj_key.split('/')[-1]
                    suffix = obj_key.split('/', 4)[-1]
                    obj_key = 'com_globe_dh_shared_mutables_assignedproduct_assignedproductmutable_test/' + suffix
                    df = pd.read_parquet(io.BytesIO(obj.get()["Body"].read()))

                    
                    columns_to_convert = ["provisiondate", "orderstartdate"]
                    for column in columns_to_convert:
                        if column in df.columns:
                            df[column] = df[column].astype(str)
                    

                    # Convert the dataframe to parquet format
                    parquet_buffer = io.BytesIO()
                    df.to_parquet(parquet_buffer, compression="snappy")

                    print("---- olympus_adh_tables")
                    print("obj_key: ", obj_key)
                    print(f"converted/compute/opt_adh/data/{obj_key}")

                    # Upload the modified parquet file to the new bucket
                    parquet_buffer.seek(0)
                    self.s3_client.upload_fileobj(parquet_buffer, self.NEW_BUCKET_NAME, f"converted/compute/opt_adh/data/{obj_key}")

                    del df
                    parquet_buffer.close()

    def load_partitions(self):
        """load new Hive partitions into a partitioned table"""
        print("--- Load partitions")
        views = ["cbrm_datamart_assignedproduct_edo"]
        for view in views:
            athena_client = boto3.client("athena")

            config = {
                "OutputLocation": f"s3://{self.NEW_BUCKET_NAME}/output/temp/",
                "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
            }
            sql = f"MSCK REPAIR TABLE {self.DATABASE_NAME}.{view}"
            context = {"Database": self.DATABASE_NAME}
            athena_client.start_query_execution(
                QueryString=sql,
                QueryExecutionContext=context,
                ResultConfiguration=config,
            )

    def process(self):
        self.csv_to_parquet()
        self.load_partitions()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
