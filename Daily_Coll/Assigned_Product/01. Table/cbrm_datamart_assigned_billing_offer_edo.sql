CREATE EXTERNAL TABLE `cbrm_datamart_assigned_billing_offer_edo`(
  `sourcelastreadtime_` bigint, 
  `sourcelastupdatetime_` bigint, 
  `assignedbillingofferkey` string, 
  `assignedbillingofferid` string, 
  `assignedbillingofferversionid` string, 
  `assignedmaincomponentkey` string, 
  `maincomponentkey` string, 
  `assignedmaincomponentid` string, 
  `assignedmaincomponentversionid` string, 
  `assignedbillingofferstatekey` string, 
  `assignedbillingofferstatuskey` string, 
  `bsscreationdate` bigint, 
  `bssmodificationdate` bigint, 
  `customerkey` string, 
  `enddate` bigint, 
  `modificationemployeekey` string, 
  `offerinstanceid` string, 
  `orderactionkey` string, 
  `parentproductcatalogkey` string, 
  `productcatalogversionkey` string, 
  `productcatalogkey` string, 
  `startdate` bigint, 
  `subscriberkey` string, 
  `migratedfromassignedbillingofferkey` string, 
  `migratedtoassignedbillingofferkey` string, 
  `itemactiontypeind` string, 
  `assignedproductkey` string, 
  `commitmentduration` double, 
  `commitmentstartdate` double, 
  `provisioningdate` string, 
  `offerlevel` string, 
  `boltind` string, 
  `pmonthlydate` bigint, 
  `dmlind` string, 
  `assignedmaincomponentstatuskey` string, 
  `deleteindicator_` boolean, 
  `creationtimestamp_` bigint)
PARTITIONED BY ( 
  `p_date` string, 
  `p_workflow_id` string, 
  `p_attempt` int, 
  `p_state` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/compute/opt_adh/data/com_globe_dh_shared_mutables_assignedbillingoffer_assignedbillingoffermutable'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')