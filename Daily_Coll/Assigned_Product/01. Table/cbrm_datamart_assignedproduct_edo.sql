CREATE EXTERNAL TABLE `cbrm_datamart_assignedproduct_edo`(
  `sourcelastreadtime_` bigint, 
  `sourcelastupdatetime_` bigint, 
  `assignedproductkey` string, 
  `customerkey` string, 
  `subscriberkey` string, 
  `assignedproductfinalstatuskey` string, 
  `assignedproductstatuskey` string, 
  `assignedproductstatekey` string, 
  `productcatalogkey` string, 
  `productcatalogversionkey` string, 
  `maincomponentflag` string, 
  `maincomponentkey` string, 
  `productofferkey` string, 
  `parentassignedproductkey` string, 
  `committedflag` string, 
  `commitmentperiod` int, 
  `equipmentcatalogkey` string, 
  `temporaryflag` string, 
  `bulkflag` string, 
  `assignedproductid` string, 
  `assignedproductreasonkey` string, 
  `ordermodeind` string, 
  `orderactionkey` string, 
  `assignedproductversionid` string, 
  `bsscreationdate` bigint, 
  `bssmodificationdate` bigint, 
  `provisiondate` string, 
  `billstartdate` double, 
  `orderstartdate` string, 
  `expirationdate` double, 
  `commitmentstartdate` double, 
  `commitmentenddate` double, 
  `bulkquantitynumber` bigint, 
  `saleschannelkey` string, 
  `installaddresskey` string, 
  `enddate` bigint, 
  `startdate` bigint, 
  `servicetypeind` string, 
  `serviceid` string, 
  `addonofferkey` string, 
  `itemactiontypeind` string, 
  `modificationemployeekey` string, 
  `orderkey` string, 
  `primaryserviceid` string, 
  `deliverymethodkey` string, 
  `currentflag` string, 
  `pmonthlydate` bigint, 
  `dmlind` string, 
  `bsspartitiondate` bigint, 
  `bsspartitionkey` string, 
  `assignedmaincomponentid` string, 
  `assignedmaincomponentstatuskey` string, 
  `deleteindicator_` boolean, 
  `creationtimestamp_` bigint)
PARTITIONED BY ( 
  `p_date` string, 
  `p_workflow_id` string, 
  `p_attempt` int, 
  `p_state` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/compute/opt_adh/data/com_globe_dh_shared_mutables_assignedproduct_assignedproductmutable'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')