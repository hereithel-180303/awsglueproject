SELECT DISTINCT
  abo.subscriberkey,
  CAST(FROM_UNIXTIME((prod.startdate / 1000)) AS date) AS "startdate",
  CAST(FROM_UNIXTIME((prod.enddate / 1000)) AS date) AS "enddate",
  abo.customerkey,
  prodparam.parametername,
  prodparam.parametervalue,
  prod.p_date AS "product_p_date",
  prodparam.p_date AS "parameter_p_date",
  abo.p_date AS "abo_p_date",
  current_date as insert_date
FROM cbrm_datamart_assignedproduct_edo prod
INNER JOIN cbrm_datamart_assigned_product_parameter_edo prodparam 
  ON prod.assignedproductkey = prodparam.assignedproductkey
  AND prod.p_date = prodparam.p_date
INNER JOIN cbrm_datamart_assigned_billing_offer_edo abo 
  ON prod.assignedproductkey = abo.assignedproductkey 
  AND prod.p_date = abo.p_date
  