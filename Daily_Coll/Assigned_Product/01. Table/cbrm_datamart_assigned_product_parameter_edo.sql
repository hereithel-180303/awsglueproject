CREATE EXTERNAL TABLE `cbrm_datamart_assigned_product_parameter_edo`(
  `sourcelastreadtime_` bigint, 
  `sourcelastupdatetime_` bigint, 
  `assignedproductparameterkey` string, 
  `parametername` string, 
  `parametervalue` string, 
  `productparameterkey` string, 
  `assignedproductkey` string, 
  `assignedproductid` string, 
  `assignedproductversionid` string, 
  `creationdate` bigint, 
  `updatedate` bigint, 
  `pmonthlydate` bigint, 
  `dmlind` string, 
  `deleteindicator_` boolean, 
  `creationtimestamp_` bigint)
PARTITIONED BY ( 
  `p_date` string, 
  `p_workflow_id` string, 
  `p_attempt` int, 
  `p_state` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/compute/opt_adh/data/com_globe_dh_shared_mutables_assignedproductparameter_assignedproductparametermutable'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')