CREATE TABLE tbl_below_threshold_accts (
  cust_ac_no bigint,
  balance double,
  acct_tag string,
  status string,
  tagging string,
  insert_date date)
PARTITIONED BY (`insert_date`, bucket(16, `cust_ac_no`))
LOCATION 's3://s3cbrmddv02/output/bulk_stop/tbl_below_threshold_accts'
TBLPROPERTIES (
  'table_type'='iceberg',
  'format'='parquet',
  'optimize_rewrite_delete_file_threshold'='10',
  'write_target_data_file_size_bytes'='536870912'
);