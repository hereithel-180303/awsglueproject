import sys
import time
import pandas as pd
import boto3
import datetime
#import openpyxl
#from openpyxl import Workbook
#from openpyxl.utils.dataframe import dataframe_to_rows

from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment, Font


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3cbrmddv02"
        self.DATABASE_NAME = "cbrm_dev_bulk_stop"
        
        self.parameter = '2023-01-04'
        
        #current_date
        #self.parameter = datetime.datetime.now().strftime('%Y-%m-%d')

    def run_athena_script(self):
        """Run SQL script to populate target table with the latest subid faid pair hist"""
        print("--- Run SQL script to populate target table")

        athena_client = boto3.client("athena")

        config = {
            "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
        }

        # Retrieve the SQL script from the S3 bucket
        bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
        obj = bucket.Object("sql_script/bulk_stop/bulk_stop.sql")
        script = obj.get()["Body"].read().decode("utf-8")

        # Split the script into separate statements
        statements = script.split(";")

        # Execute each statement individually
        for statement in statements:
            # Trim leading and trailing whitespaces
            statement = statement.strip()

            # Skip empty statements and comments
            if not statement or statement.startswith("--") or statement.startswith("/*"):
                continue

            # Replace the parameter placeholder with the actual parameter value
            sql = statement.replace("?", f"'{self.parameter}'")

            context = {"Database": self.DATABASE_NAME}
            res = athena_client.start_query_execution(
                QueryString=sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )

            print(res)

            query_execution_id = res["QueryExecutionId"]
            status = self.wait_for_query_completion(query_execution_id, athena_client)
            print(status)

            # Export the query result to a CSV file
            result_file = self.export_query_result_to_csv(query_execution_id, athena_client)
            print(f"Result file: {result_file}")
            
            
            # Generate input files for each iteration
            div = 3000
            data = pd.read_csv(result_file, header=None)
            
            
            # Add the 'cust_ac_no' header
            data.columns = ['cust_ac_no']

            
            # Calculate variables using Python list operations
            numaccts = len(data)
            iterations = (numaccts - 1) // div + 1
            
            print("Num Accts:", numaccts)
            print("Iterations:", iterations)
            
            for i in range(1, iterations + 1):
                j = i - 1
            
                print("Processing iteration:", i)
            
                part_data = data["cust_ac_no"].iloc[div * j: div * i].dropna().astype(str)
                part_data.reset_index(drop=True, inplace=True)  # Reset index
                print("part_data:", part_data)  # Debug statement
            
                part_df = pd.DataFrame(
                    {"cust_ac_no": part_data}, index=range(len(part_data))
                )
            
                print("part_df:", part_df)  # Debug statement
            
                max_cnt = len(part_data)
            
                temp_base_df = pd.DataFrame(
                    {
                        "temp": part_df["cust_ac_no"].apply(
                            lambda x: f"2;{x};STCL;;;FORSTL"
                        )
                    },
                    index=range(max_cnt),
                )
                parameter_datetime = datetime.datetime.strptime(self.parameter, "%Y-%m-%d")
                temp1_df = pd.DataFrame({"temp": [f"1;{parameter_datetime.strftime('%Y%m%d')}"]}, index=[0])
                temp2_df = pd.DataFrame({"temp": [f"3;{max_cnt}"]}, index=[0])
            
                finlist_df = pd.concat([temp1_df, temp_base_df, temp2_df], axis=0)
            
                part_df.to_csv(f"Part{i}.csv", index=False)
                temp_base_df.to_csv(f"temp_base_{i}.csv", index=False)
                temp1_df.to_csv(f"temp1_{i}.csv", index=False)
                temp2_df.to_csv(f"temp2_{i}.csv", index=False)
            
                finlist_df.to_csv(f"finlist_{i}.csv", index=False)
                
                s3_path = f"s3://{self.BUCKET_NAME}/output/bulk_stop/cl9masstrx_files/CL9MASSTRX_{parameter_datetime.strftime('%Y%m%d')}_14000{i}.input"
                finlist_df.to_csv(
                    s3_path,
                    sep="\t",
                    header=False,
                    index=False,
                )
                print(f"File saved to S3: {s3_path}")


        # Execute below_threshold.sql script
        obj = bucket.Object("sql_script/bulk_stop/below_threshold.sql")
        script = obj.get()["Body"].read().decode("utf-8")

        # Split the script into separate statements
        statements = script.split(";")

        # Execute each statement individually
        for statement in statements:
            # Trim leading and trailing whitespaces
            statement = statement.strip()

            # Skip empty statements and comments
            if not statement or statement.startswith("--") or statement.startswith("/*"):
                continue

            # Replace the parameter placeholder with the actual parameter value
            sql_statement = statement.replace("?", f"'{self.parameter}'")
            
                   
            # Prepare the SQL statement
            prepare_sql = f"PREPARE insert_tbl_below_threshold_accts FROM INSERT INTO tbl_below_threshold_accts {sql_statement};"
            print(prepare_sql)
            
            # Start query execution for PREPARE statement
            res_prepare = athena_client.start_query_execution(
                QueryString=prepare_sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )
            print(res_prepare)

            
            res = athena_client.start_query_execution(
                QueryString=sql_statement,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )
            print(res)

            xlx_query_execution_id = res["QueryExecutionId"]
            status = self.wait_for_query_completion(xlx_query_execution_id, athena_client)
            print(status)

            # Export the query result to an XLSX file
            result_file = self.export_query_result_to_xlsx(xlx_query_execution_id, athena_client)
            print(f"Result file: {result_file}")
            

            # Execute the prepared statement
            execute_sql = f"EXECUTE insert_tbl_below_threshold_accts;"
            print(execute_sql)
            
            # Start query execution for EXECUTE statement
            res_execute = athena_client.start_query_execution(
                QueryString=execute_sql,
                QueryExecutionContext=context,
                ResultConfiguration=config
            )
            print(res_execute)

    def wait_for_query_completion(self, query_execution_id, athena_client):
        """Wait for the query execution to complete"""
        while True:
            response = athena_client.get_query_execution(QueryExecutionId=query_execution_id)
            status = response['QueryExecution']['Status']['State']
    
            if status in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
                break  # Break the loop when the query execution is completed
    
            time.sleep(5)  # Wait for 5 seconds before checking the status again
    
        return status

    def export_query_result_to_csv(self, query_execution_id, athena_client):
        """Export the query result to a CSV file"""
        # Get the query results metadata
        query_results = athena_client.get_query_results(QueryExecutionId=query_execution_id)

        # Extract column names from the results metadata
        column_names = [field['Name'] for field in query_results['ResultSet']['ResultSetMetadata']['ColumnInfo']]

        # Initialize a list to store the rows
        rows = []

        # Paginate through the query results to retrieve all rows
        next_token = None
        while True:
            if next_token:
                query_results = athena_client.get_query_results(
                    QueryExecutionId=query_execution_id,
                    NextToken=next_token
                )
            else:
                query_results = athena_client.get_query_results(QueryExecutionId=query_execution_id)

            # Extract rows from the query results
            for row in query_results['ResultSet']['Rows']:
                rows.append([field.get('VarCharValue', '') for field in row['Data']])

            # Check if there are more results to retrieve
            next_token = query_results.get('NextToken')
            if not next_token:
                break

        # Create a DataFrame from the rows and column names
        df = pd.DataFrame(rows, columns=column_names)
        
        # Remove the header "cust_ac_no" from the DataFrame
        df = df[df["cust_ac_no"] != "cust_ac_no"]
        
        # Define the output file path
        parameter_datetime = datetime.datetime.strptime(self.parameter, "%Y-%m-%d")
        output_file_path = f"s3://{self.BUCKET_NAME}/output/bulk_stop/bulk_stop_files/bulk_stop_{parameter_datetime.strftime('%Y%m%d')}.csv"
        
        
        # Save the DataFrame as a CSV file in S3
        df.to_csv(output_file_path, index=False, header=False)

        return output_file_path

    def export_query_result_to_xlsx(self, xlx_query_execution_id, athena_client):
        """Wait for the query execution to complete"""
        while True:
            response = athena_client.get_query_execution(QueryExecutionId=xlx_query_execution_id)
            status = response['QueryExecution']['Status']['State']

            if status in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
                break  # Break the loop when the query execution is completed

            time.sleep(5)  # Wait for 5 seconds before checking the status again

        return status


    def export_query_result_to_xlsx(self, xlx_query_execution_id, athena_client):
        """Export the query result to an XLSX file"""
        # Get the query results metadata
        query_results = athena_client.get_query_results(QueryExecutionId=xlx_query_execution_id)
    
        # Extract column names from the results metadata
        column_names = [field['Name'] for field in query_results['ResultSet']['ResultSetMetadata']['ColumnInfo']]
    
        # Initialize a list to store the rows
        rows = []
    
        # Paginate through the query results to retrieve all rows
        next_token = None
        while True:
            if next_token:
                query_results = athena_client.get_query_results(
                    QueryExecutionId=xlx_query_execution_id,
                    NextToken=next_token
                )
            else:
                query_results = athena_client.get_query_results(QueryExecutionId=xlx_query_execution_id)
    
            # Extract rows from the query results
            for row in query_results['ResultSet']['Rows']:
                rows.append([field.get('VarCharValue', '') for field in row['Data']])
    
            # Check if there are more results to retrieve
            next_token = query_results.get('NextToken')
            if not next_token:
                break
    
        # Create a DataFrame from the rows and column names
        df = pd.DataFrame(rows, columns=column_names)
    
        # Define the output file path
        parameter_datetime = datetime.datetime.strptime(self.parameter, "%Y-%m-%d")
        output_file_path = f"s3://{self.BUCKET_NAME}/output/bulk_stop/below_threshold_files/Below_Threshold_Accts_{parameter_datetime.strftime('%Y%m%d')}.xlsx"
    
        # Create a new workbook
        workbook = Workbook()
    
        # Remove the default sheet created by openpyxl
        workbook.remove(workbook.active)
    
        # Create a dictionary to store the dataframes for each sheet
        sheet_data = {}
    
        # Loop through each sheet name
        for sheet_name in ['wless', 'wline', 'bayan']:
            # Filter the DataFrame based on the condition
            filtered_df = df[df['acct_tag'] == sheet_name]
            # Drop the 'insert_date' column
            filtered_df = filtered_df.drop(columns=['insert_date'])
            # Store the filtered DataFrame for the sheet
            sheet_data[sheet_name] = filtered_df
               
    
        # Filter the DataFrame for the 'non_bss' sheet
        non_bss_df = df[df['acct_tag'].isnull() | (df['acct_tag'] == '')]
        # Store the filtered DataFrame for the 'non_bss' sheet
        non_bss_df = non_bss_df.drop(columns=['balance', 'acct_tag', 'status', 'tagging', 'insert_date'])
        sheet_data['non_bss'] = non_bss_df 
    
        # Save the DataFrame to the corresponding sheets
        with pd.ExcelWriter(output_file_path, engine='openpyxl') as writer:
            writer.book = workbook
            writer.sheets = dict((ws.title, ws) for ws in workbook.worksheets)
    
            for sheet_name, sheet_df in sheet_data.items():
                sheet_df.to_excel(writer, sheet_name=sheet_name, index=False, header=True)
                
                # Get the worksheet
                worksheet = writer.sheets[sheet_name]
                
                # Get the number of columns in the DataFrame
                num_columns = len(sheet_df.columns)
    
                # Get the header range
                header_range = f"A1:{get_column_letter(num_columns)}1"
            
                # Remove bold formatting from the header cells and align them to the left
                for row in worksheet[header_range]:
                    for cell in row:
                        cell.font = Font(bold=False)
                        cell.alignment = Alignment(horizontal='left')
    
            writer.save()
    
        return output_file_path

if __name__ == "__main__":
    job = GlueJob()
    job.run_athena_script()