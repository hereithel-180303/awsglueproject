PREPARE insert_tbl_below_threshold_accts FROM
INSERT INTO tbl_below_threshold_accts
with params as
(
	select ? as param 
), not_BSS as
(
	select 
		vh.cust_ac_no,
	    vh.filename
	from 
		view_collections_hca vh
	where 
		not exists (select 
						cust_ac_no 
					  from 
						view_bulk_stop_current vcgl
					  where 
						vh.cust_ac_no = vcgl.cust_ac_no
					)
	and ((date_parse("regexp_extract"(vh.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') = (select CAST(param as date) from params)))-- current_date 
), tag as (
select 
    distinct 
	mybss_aging.afilename,
	mybss_aging.cust_ac_no,
	mybss_aging.balance,
	mybss_aging.acct_tag, 
	mybss_aging.status,
	case 
		when exists (
			select 
				cust_ac_no
			from 
				view_bulk_stop vbs	-- view_bulk_stop PREVIOUS Date
			where
				mybss_aging.cust_ac_no = vbs.cust_ac_no
				and date_parse(regexp_extract(vbs.filename, '([0-9])\d{3}(0[1-9]|1[0-2])(0[1-9])'), '%Y%m%d') = (select CAST(param as date)  - interval '1' day from params) -- current_date - interval '1' day
			) 
			then 'prev bulk stop'
		else 
			'below threshold' 
	end as tagging
	from(
		select 
			a.filename as afilename,
			b.filename as gfilename,
			a.cust_ac_no,
			b.balance,
			CASE 
				WHEN regexp_like(b.filename, 'wireless') THEN 'wless'
				WHEN regexp_like(b.filename, 'innove')   THEN 'wline' 
				WHEN regexp_like(b.filename, 'bayan')    THEN 'bayan'
			END as acct_tag,
			b.status_2 status
		from 
			not_BSS a
		left join 
			aging_financial_account.view_financial_account_balance b
		on 
			a.cust_ac_no=b.financial_account_id
	) mybss_aging
where 
	cast(regexp_extract(mybss_aging.gfilename, '\d{4}-\d{2}-\d{2}') as date) = (select CAST(param as date)  - interval '1' day from params) -- current_date - interval '1' day
	and date_parse("regexp_extract"(mybss_aging.afilename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') =  (select CAST(param as date) from params) -- current_date as date
) -- END OF CTE
select 
	below_threshold_accts.*,
	current_date as inser_date
from 
(
	select
		distinct
		t.cust_ac_no,
		t.balance,
		t.acct_tag, 
		t.status,
		t.tagging
	from 
		tag t
	UNION ALL
	select 
		distinct 
		cust_ac_no,
		NULL balance,
		NULL acct_tag, 
		NULL status,
		NULL tagging
	from  
		not_BSS nbss
	where 
		not exists (select 
						t.cust_ac_no 
					from 
						tag t
					where 
						nbss.cust_ac_no = t.cust_ac_no
					)
) as below_threshold_accts