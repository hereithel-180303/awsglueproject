with params as
(
	select ? as param 
)	
SELECT 
	DISTINCT mybss_aging.cust_ac_no
FROM
  (
   SELECT
     a.filename afilename
   , b.filename gfilename
   , a.cust_ac_no
   , b.balance
   , b.status_2 
   FROM
     view_collections_hca a
   LEFT JOIN 
	 aging_financial_account.view_financial_account_balance b
   ON 
	(a.cust_ac_no = b.financial_account_id) 
   WHERE ((NOT (b.status_2 IN ('CAN', 'CAN_CREDIT')))
		AND (b.balance > 0)
		AND (NOT EXISTS					
				(SELECT 
					cust_ac_no
				FROM
					view_bulk_stop vbs -- view_bulk_stop PREVIOUS Date
				WHERE
					a.cust_ac_no = vbs.cust_ac_no
					AND(date_parse(regexp_extract(vbs.filename, '([0-9])\d{3}(0[1-9]|1[0-2])(0[1-9])'), '%Y%m%d') = (select CAST(param as date)  - interval '1' day from params)) -- current_date - interval '1' day
				)
			)		
		AND (CAST(regexp_extract(b.filename, '\d{4}-\d{2}-\d{2}') AS date) = (select CAST(param as date)  - interval '1' day from params)) -- current_date - interval '1' day
	)
)  mybss_aging
WHERE (date_parse("regexp_extract"(mybss_aging.afilename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') = (select CAST(param as date) from params)) -- current_date