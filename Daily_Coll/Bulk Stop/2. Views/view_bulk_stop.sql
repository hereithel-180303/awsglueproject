CREATE OR REPLACE VIEW "view_bulk_stop" AS 
SELECT
  CAST(cust_ac_no AS bigint) cust_ac_no
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  bulk_stop
