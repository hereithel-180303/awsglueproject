CREATE OR REPLACE VIEW "view_collections_hca" AS 
SELECT
  CAST(trim(BOTH FROM REPLACE(COALESCE(cust_ac_no, ''), CHR(160), '')) AS BIGINT) cust_ac_no
, dt
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  collections_hca
