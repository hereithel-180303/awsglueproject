SELECT DISTINCT
  abo.subscriberkey
, CAST(FROM_UNIXTIME((prod.startdate / 1000)) AS date) "startdate"
, CAST(FROM_UNIXTIME((prod.enddate / 1000)) AS date) "enddate"
, abo.customerkey
, prodparam.parametername
, prodparam.parametervalue
, prod.p_date "product_p_date"
, prodparam.p_date "parameter_p_date"
, abo.p_date "abo_p_date"
FROM
  ((cbrm_datamart_assignedproduct_edo prod
INNER JOIN cbrm_datamart_assigned_product_parameter_edo prodparam ON (prod.assignedproductkey = prodparam.assignedproductkey))
INNER JOIN cbrm_datamart_assigned_billing_offer_edo abo ON (abo.assignedproductkey = prod.assignedproductkey))
WHERE ((abo.p_state = 'published') AND (prod.p_state = 'published') AND (prodparam.p_state = 'published'))
