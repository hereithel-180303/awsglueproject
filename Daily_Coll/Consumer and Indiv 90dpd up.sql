WITH consumer as
(
	select 
		distinct 
		 b.financial_account_id cust_ac_no
		,b.aging_grp
		,b.balance
		,b.status_2
		,b.last_payment_date
		,b.last_payment_amount
		,sum(b.bucket10_270_days 
						+ b.bucket9_240_269_days
						+ b.bucket8_210_239_days
						+ b.bucket7_180_209_days
						+ b.bucket6_150_179_days
						+ b.bucket5_120_149_days
						+ b.bucket4_90_119_days
						+ b.bucket3_60_89_days
						+ b.bucket2_30_59_days
						+ b.bucket1_1_29_days) as overdue 
		,b.account_salutation
		,b.account_first_name
		,b.account_middle_name
		,b.account_last_name
		,b.account_type
		,b.account_sub_type
		,b.collection_status
		,b.email_address
		,b.postal_code
		,b.company_name
		,b.entity_status
		,b.pd_date
		,b.days_past_due
		,case 
			when regexp_like(b.filename, 'wireless') then 'wless'
			when regexp_like(b.filename, 'innove') then 'wline'
		end as tag
		,case 
			when exists (select 
							vce.cust_ac_no 
						from 
							view_collections_exception vce
						where
							b.financial_account_id = vce.cust_ac_no
							and date_parse(regexp_extract(vce.filename, '\d{4}\d{2}\d{2}'), '%Y%m%d') = cast('2023-01-09' as date)
												/*
											    where  cast(regexp_extract(filename, '\d{4}\d{2}\d{2}') as date) = (
												select max(cast(regexp_extract(filename, '\d{4}\d{2}\d{2}') as date)) from view_collections_exception
												)
												*/
						) then 'Y' 
		else 'N' 
		end as part_of_exception
		,case 
			when exists(select 
							vcha.cust_ac_no 
						from 
							view_collections_hold_action vcha
						where
							b.financial_account_id = vcha.cust_ac_no
							and date_parse(regexp_extract(vcha.filename, '\d{4}\d{2}\d{2}'), '%Y%m%d') = cast('2023-01-09' as date)
						/*
						   where  cast(regexp_extract(filename, '\d{4}\d{2}\d{2}') as date) = (
						select max(cast(regexp_extract(filename, '\d{4}\d{2}\d{2}') as date)) from view_collections_hold_action
						)
						*/	
						) then 'Y' 
			else 'N' 
		end as part_of_hca
		,case 
			when exists (select 
							vcpa.cust_ac_no 
						from view_collections_plat_accounts vcpa /*Overwrite*/ 
						where 
							b.financial_account_id = vcpa.cust_ac_no
						) then 'Y' 
			else 'N' 
		end as part_of_platinum
	from 
		view_aging_mybss b
	where 
		b.account_type in ('C', 'S', 'E')
		and b.status_2 not in ('C', 'CAN', 'CAN_CREDIT')
		and b.aging_grp > 5
		and cast(regexp_extract(b.filename, '\d{4}-\d{2}-\d{2}') as date) = CAST('2023-04-09' AS date)
		/*
		and cast(regexp_extract(b.filename, '\d{4}-\d{2}-\d{2}') as date) in (
		select max(cast(regexp_extract(c.filename, '\d{4}-\d{2}-\d{2}') as date)) from view_aging_mybss
		)
		*/
	GROUP BY 
		 b.filename 
		,b.financial_account_id 
		,b.aging_grp
		,b.balance
		,b.status_2 
		,b.last_payment_date
		,b.last_payment_amount
		,b.bucket10_270_days 
		,b.bucket9_240_269_days
		,b.bucket8_210_239_days
		,b.bucket7_180_209_days
		,b.bucket6_150_179_days
		,b.bucket5_120_149_days
		,b.bucket4_90_119_days
		,b.bucket3_60_89_days
		,b.bucket2_30_59_days
		,b.bucket1_1_29_days
		,b.account_salutation
		,b.account_first_name
		,b.account_middle_name
		,b.account_last_name
		,b.account_type
		,b.account_sub_type
		,b.collection_status
		,b.email_address
		,b.postal_code
		,b.company_name
		,b.entity_status
		,b.pd_date
		,b.days_past_due
), nsp_wline_wless as
(
	select 
		distinct nsp.*, 
		case 
			when exists (select 
							vnwn.cust_ac_no  
						from 
							view_nsp_wireless_new vnwn
						where 
							nsp.financial_account_id = vnwn.cust_ac_no
							and vnwn.nsp_tag= '1' 
							and vnwn.cohort = '202304'
						/*
						and cohort  = (
						select max(regexp_extract(a.filename, '\d{3}\d{3}')) from view_nsp_b a
						) 
						*/ 
						) then 'NSP'
			when exists (select 
							vnb.cust_ac_no  
						from 
							view_nsp_b vnb
						where 
							nsp.financial_account_id = vnb.cust_ac_no
							and vnb.nsp_tag= '1' 
							and vnb.cohort = '202304'
							/*
							and cohort  = (
							select max(regexp_extract(b.filename, '\d{3}\d{3}')) from view_nsp_b b
							) 
							*/ 
						) then 'NSP'
			else 'Non-NSP' 
		end as NSP_Tagging
	from (
		select 
			vam.filename,
			vam.financial_account_id,
			vam.status_2,
			vam.account_type
		from 
			view_aging_mybss vam
		) as nsp	
		where 
			nsp.status_2 not in ('CAN','CAN_CREDIT') -- status_2
			and nsp.account_type = 'C'
			and cast(regexp_extract(nsp.filename, '\d{4}-\d{2}-\d{2}') as date) = CAST('2023-04-09' AS date)
			/*
			and cast(regexp_extract(nsp.filename, '\d{4}-\d{2}-\d{2}') as date) = (
			select max(cast(regexp_extract(c.filename, '\d{4}-\d{2}-\d{2}') as date)) from view_aging_mybss
			) 
			*/
), base_plan as (
	select distinct *, 	
		case 
			when vbp.billing_offer_name like '%myStarter%' then 'ltp'
			when vbp.billing_offer_name like '%LOAD TIPID%' then 'ltp' 
		else 'others' 
	end as tagging
	from 
		view_base_plan vbp
	where 
		exists (select 
					c.cust_ac_no 
				from 
				consumer c
				where 
					vbp.financial_account_id = c.cust_ac_no
				)
		and regexp_extract(vbp.filename, '\d{3}\d{3}') = '202304'
		/*
			and cast(regexp_extract(vbp.filename, '\d{3}\d{3}') as date)  = (
			select max(regexp_extract(b.filename, '\d{3}\d{3}')) from view_nsp_b b
			) 
		*/ 
), list as (
SELECT 
	c_w_nsp.*
from (
	select 
		distinct c.*
		,case 
			when c.account_type in ('S') and c.account_sub_type in ('D', 'U', 'V') then 'SG_Indiv' 
			when c.account_type in ('S', 'E') and c.account_sub_type in ('I') then 'EG_Indiv' 
			when exists (select 
							vpa.cust_ac_no 
						from 
							view_collections_plat_accounts vpa /*Overwrite*/
						where 
							c.cust_ac_no = vpa.cust_ac_no 
						) then 'Platinum Managed'
			when exists (select 
							vsl.cust_ac_no 
						from 
							view_shp_list vsl
						where 
							c.cust_ac_no = vsl.cust_ac_no 
							and regexp_extract(filename, '\d{3}\d{3}') = '202304'
							/* and max(regexp_extract(filename, '\d{3}\d{3}')) = ( select max(regexp_extract(a.filename, '\d{3}\d{3}')) from view_shp_list a) */
						) then 'consumer_shp'
			when exists (select 
							bp.financial_account_id 
						from 
							base_plan bp
						where 
							c.cust_ac_no = bp.financial_account_id
							and bp.tagging='ltp'
						) then 'consumer_ltp'
			when exists (select 
							vtn.cust_ac_no 
						from
							view_total_nomadic vtn
						where 
							c.cust_ac_no = vtn.cust_ac_no
							and regexp_extract(filename, '\d{3}\d{3}') = '202304'
							/* and max(regexp_extract(filename, '\d{3}\d{3}')) = (select max(regexp_extract(b.filename, '\d{3}\d{3}')) from view_total_nomadic b) */
						) then 'consumer_nomadic'
			when exists (select 
							vwt.cust_ac_no 
						from 
							view_wless_tag vwt
						where 
							c.cust_ac_no = vwt.cust_ac_no
							and Nomadic_tag = 'Nomadic'
							and regexp_extract(filename, '\d{3}\d{3}') = '202304'
							/* and max(regexp_extract(filename, '\d{3}\d{3}')) = (select max(regexp_extract(c.filename, '\d{3}\d{3}')) from view_wless_tag c) */
						) then 'consumer_nomadic'
			when c.account_type in ('C') then 'consumer_MT' 
			else 'Others' 
		end as segment,
		case 
			when exists (select 
							nww.financial_account_id 
						from 
							nsp_wline_wless nww
						where 
							c.cust_ac_no = nww.financial_account_id
							and nww.NSP_Tagging = 'NSP') then 'NSP'
			else 'NOT NSP' 
		end as NSP_tag
	from 
		consumer c
	where 
		c.account_sub_type not in ('J', 'N', 'F', 'R', 'O')
	) c_w_nsp
where 
	c_w_nsp.account_type <>'E'
	and c_w_nsp.account_sub_type <> 'C'
), mobtel as (
	select 
		distinct vpmca.* 
	from 
		view_primary_msisdn_crm_active vpmca
	where 
		exists(select 
					l.cust_ac_no 
				from 
					list l
				where 
					vpmca.cust_ac_no = l.cust_ac_no
				)
)-- END OF WITH
select 
	distinct 
	a.*, 
	b.msisdn 
from list a 
left join mobtel b
on a.cust_ac_no=b.cust_ac_no
where a.segment not in ('SG_Indiv', 'EG_Indiv', 'Others')

