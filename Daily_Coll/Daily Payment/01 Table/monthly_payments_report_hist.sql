CREATE TABLE monthly_payments_report_hist (
  cfu string,
  status string,
  entity string,
  source string,
  payt decimal(38, 2),
  insert_date date)
PARTITIONED BY (`insert_date`, bucket(16, `entity`))
LOCATION 's3://s3cbrmddv02/output/monthly_payments'
TBLPROPERTIES (
  'table_type'='iceberg',
  'format'='parquet',
  'optimize_rewrite_delete_file_threshold'='10',
  'write_target_data_file_size_bytes'='536870912'
);