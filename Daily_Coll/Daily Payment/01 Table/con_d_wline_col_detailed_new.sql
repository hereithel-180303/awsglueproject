CREATE EXTERNAL TABLE `con_d_wline_col_detailed_new`(
  `cust_ac_no` string, 
  `receipt_dt` string, 
  `cashbox_dt` string, 
  `receipt_amt` string, 
  `ac_cat` string, 
  `cust_classn` string, 
  `ac_stat` string, 
  `ac_cat_desc` string, 
  `cust_classn_desc` string, 
  `fran_area` string)
PARTITIONED BY ( 
  `dt` string, 
  `entity` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/con_d_wline_col_detailed_new'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')