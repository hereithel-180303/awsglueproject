CREATE EXTERNAL TABLE `mybss_lookup`(
  `report_grouping` string COMMENT 'from deserializer', 
  `customer_type` string COMMENT 'from deserializer', 
  `customer_sub_type` string COMMENT 'from deserializer', 
  `customer_type_description` string COMMENT 'from deserializer', 
  `customer_sub_type_description` string COMMENT 'from deserializer', 
  `class` string COMMENT 'from deserializer', 
  `new` string COMMENT 'from deserializer', 
  `accounting_group` string COMMENT 'from deserializer', 
  `date_created` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/lookup/mybss_lookup'
TBLPROPERTIES (
  'classification'='csv', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1685002001')