CREATE EXTERNAL TABLE `bfm_payments`(
  `payment_source_id` string, 
  `account_from` string, 
  `account_to` string, 
  `l9_msisdn` string, 
  `amount_from` string, 
  `original_amount` string, 
  `transferred_amount` string, 
  `deposit_date` string, 
  `activity_date` string, 
  `payment_method` string, 
  `credit_id` string, 
  `payment_id` string, 
  `l9_or_id` string, 
  `confirmation_no` string, 
  `check_no` string, 
  `check_drawer_name` string, 
  `l9_file_name` string, 
  `bank_code` string)
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/bfm_payment'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')