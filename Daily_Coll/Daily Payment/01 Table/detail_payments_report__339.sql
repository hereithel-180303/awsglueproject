CREATE EXTERNAL TABLE `detail_payments_report__339`(
  `account number` string, 
  `primary msisdn` string, 
  `salutation` string, 
  `first name` string, 
  `middle name` string, 
  `last name` string, 
  `corporate name` string, 
  `account type` string, 
  `account sub-type` string, 
  `status` string, 
  `posting date` string, 
  `receipt date` string, 
  `receipt amount` string, 
  `payment method` string, 
  `corp id` string, 
  `payment source` string, 
  `payment source id` string, 
  `reference or or number` string, 
  `outstanding balance` string, 
  `cheque number` string, 
  `payment id` string, 
  `credit id` string, 
  `payment reference id` string, 
  `sap store code` string, 
  `account currency` string, 
  `payment currency` string, 
  `amount in payment currency` string, 
  `amount in peso` string, 
  `zero tag` string)
PARTITIONED BY ( 
  `dt` string, 
  `entity` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/daily_payment_339/'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')