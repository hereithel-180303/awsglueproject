CREATE EXTERNAL TABLE `gbu_wless_eg`(
  `cust_ac_no` string, 
  `company_name` string, 
  `assignee_name` string, 
  `postal_code` string, 
  `balance` string, 
  `overdue` string, 
  `balance_90up` string, 
  `cfwd_balance` string, 
  `last_payment_date` string, 
  `last_payment_amount` string, 
  `bucket0___current` string, 
  `bucket1___1_29_days` string, 
  `bucket2___30_59_days` string, 
  `bucket3___60_89_days` string, 
  `bucket4___90_119_days` string, 
  `bucket5___120_149_days` string, 
  `bucket6___150_179_days` string, 
  `bucket7___180_209_days` string, 
  `bucket8___210_239_days` string, 
  `bucket9___240_269_days` string, 
  `bucket10___270__days` string, 
  `last_payment_amount___original` string, 
  `collection_status` string, 
  `aging_grp` string, 
  `sub_type_desc` string, 
  `acctg_grp_new` string, 
  `status_2` string, 
  `account_type` string, 
  `class` string, 
  `company_code` string, 
  `gbu_name_final` string, 
  `cluster_final` string, 
  `industry_final` string, 
  `am_final` string, 
  `analyst_final` string, 
  `tier_final` string, 
  `agency_final` string, 
  `gbu_nsp_3mos_tag` string, 
  `gbu_nsp_6mos_tag` string, 
  `payt_last_3mos` string, 
  `payt_last_6mos` string, 
  `rc_202211` string, 
  `payments_202301` string, 
  `cust_id` string, 
  `bill_summ_id` string, 
  `bill_cutoff_date` string, 
  `ar_age_202301` string, 
  `billing_offer_name` string, 
  `vendor` string, 
  `entity_status` string, 
  `days_past_due` string, 
  `exception` string, 
  `activation` string, 
  `activation_month` string, 
  `fa_mod` string, 
  `conso_gbu` string, 
  `mybss_tag` string, 
  `dts_cnt` string, 
  `dts_amt` string, 
  `hotlist_tag` string, 
  `loar` string)
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/gbu/wless_eg'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')