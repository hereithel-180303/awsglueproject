CREATE EXTERNAL TABLE `iccbs_lookup`(
  `acctg_grp` string COMMENT 'from deserializer', 
  `classification` string COMMENT 'from deserializer', 
  `cust_classn` string COMMENT 'from deserializer', 
  `ac_cat` string COMMENT 'from deserializer', 
  `ac_cat_desc` string COMMENT 'from deserializer', 
  `acct_grp` string COMMENT 'from deserializer', 
  `ao_cohort` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/lookup/iccbs_lookup'
TBLPROPERTIES (
  'classification'='csv', 
  'skip.header.line.count'='1')