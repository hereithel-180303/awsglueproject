CREATE EXTERNAL TABLE `cml_line_level`(
  `num` string, 
  `group_id` string, 
  `gbu_code` string, 
  `gbu_segment` string, 
  `salesforce_id` string, 
  `account_id` string, 
  `customer_id` string, 
  `subscriber_id` string, 
  `msisdn` string, 
  `financial_account_id` string, 
  `customer_alias` string, 
  `final_account_name` string, 
  `customer_sub_type_desc` string, 
  `industry` string, 
  `address_id` string, 
  `company_address` string, 
  `company_address_zip` string, 
  `full_installation_address` string, 
  `installation_zip` string, 
  `source` string, 
  `product_group` string, 
  `product_type` string, 
  `mpid` string, 
  `main_plan_name` string, 
  `main_plan_msf_vatex` string, 
  `monthly_recurring_charge` string, 
  `billed_rev_uc_oc` string, 
  `otc_ots` string, 
  `final_msf` string, 
  `plan_mix` string, 
  `bandwidth` string, 
  `cabinet` string, 
  `distribution_point_id` string, 
  `subscriber_activation_date` string, 
  `subscriber_tenure_months` string, 
  `contract_start_date` string, 
  `contract_end_date` string, 
  `contract_status` string, 
  `cml_type` string, 
  `ict_product_category` string, 
  `product_name` string, 
  `charge_type` string, 
  `otc_year` string, 
  `am_assignment` string, 
  `sales_cluster` string, 
  `sales_region` string, 
  `status_new_or_existing` string, 
  `tier` string, 
  `service_type` string, 
  `technology` string, 
  `service_group` string, 
  `credit_limit` string, 
  `outstanding_balance` string, 
  `ob_bucket` string, 
  `bill_cutoff` string, 
  `segment` string, 
  `lat` string, 
  `long` string, 
  `fta_ada_ali (for the month)` string)
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/cml_line_level'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')