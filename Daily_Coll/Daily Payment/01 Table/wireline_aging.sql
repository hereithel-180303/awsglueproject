CREATE EXTERNAL TABLE `wireline_aging`(
  `cust_ac_no` string, 
  `ac_stat` string, 
  `cust_classn` string, 
  `cust_classn_desc` string, 
  `ac_cat` string, 
  `ac_cat_desc` string, 
  `last_name` string, 
  `first_name` string, 
  `fran_area` string, 
  `os_balance` string, 
  `credits` string, 
  `olt30days` string, 
  `o30to60days` string, 
  `o60to90days` string, 
  `o90to120days` string, 
  `o120to150days` string, 
  `o150to180days` string, 
  `o180to210days` string, 
  `ogt210days` string, 
  `currency_cd` string, 
  `age` string, 
  `service` string, 
  `acctg_grp_old` string, 
  `classification_old` string, 
  `acctg_grp` string, 
  `classification` string, 
  `inst_st_dt` string, 
  `salesman_id` string, 
  `postal_cd` string, 
  `province` string, 
  `bill_postal_cd` string, 
  `tenure` string, 
  `region` string, 
  `prod_id` string, 
  `prod_name` string, 
  `svc_type` string, 
  `package` string, 
  `product` string, 
  `tenure_group` string)
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/lookup/wireline_aging'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')