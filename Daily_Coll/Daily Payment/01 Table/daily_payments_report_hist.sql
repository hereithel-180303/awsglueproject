CREATE TABLE daily_payments_report_hist (
  financial_account_id bigint,
  cfu string,
  status string,
  entity string,
  source string,
  posting_date string,
  payt decimal(38, 2),
  gbu_name string,
  runrate bigint,
  insert_date date)
PARTITIONED BY (`insert_date`, bucket(16, `financial_account_id`))
LOCATION 's3://s3cbrmddv02/output/daily_coll/payments/daily_payments'
TBLPROPERTIES (
  'table_type'='iceberg',
  'format'='parquet',
  'optimize_rewrite_delete_file_threshold'='10',
  'write_target_data_file_size_bytes'='536870912'
);