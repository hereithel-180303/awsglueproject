CREATE EXTERNAL TABLE `bl_rcpts`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `receipt_date` string COMMENT 'from deserializer', 
  `receipt_amt` string COMMENT 'from deserializer', 
  `posting_date` string COMMENT 'from deserializer', 
  `manual_or` string COMMENT 'from deserializer', 
  `collector_id` string COMMENT 'from deserializer', 
  `bank_sort_cd` string COMMENT 'from deserializer', 
  `source` string COMMENT 'from deserializer', 
  `ceris_no` string COMMENT 'from deserializer', 
  `user_id` string COMMENT 'from deserializer', 
  `ceris_batch_name` string COMMENT 'from deserializer', 
  `receipt_method` string COMMENT 'from deserializer', 
  `receipt_desc` string COMMENT 'from deserializer', 
  `home_curr_amt` string COMMENT 'from deserializer', 
  `curr_code` string COMMENT 'from deserializer', 
  `amount` string COMMENT 'from deserializer', 
  `converted` string COMMENT 'from deserializer', 
  `cust_classn` string COMMENT 'from deserializer', 
  `ac_cat` string COMMENT 'from deserializer', 
  `ac_stat` string COMMENT 'from deserializer', 
  `company_id` string COMMENT 'from deserializer', 
  `sequesnce_no` string COMMENT 'from deserializer', 
  `cheque_no` string COMMENT 'from deserializer')
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/daily_coll/bl_rcpts'
TBLPROPERTIES (
  'serialization.null.format'='', 
  'skip.header.line.count'='1')