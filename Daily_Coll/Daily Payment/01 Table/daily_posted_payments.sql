CREATE EXTERNAL TABLE `daily_posted_payments`(
  `cust_ac_no` string, 
  `cust_name` string, 
  `ac_stat` string, 
  `bill_currency_code` string, 
  `amount` string, 
  `application_period` string, 
  `actual_payment_currency` string, 
  `actual_amt_received` string, 
  `receipt_amt` string, 
  `receipt_dt` string, 
  `post_date` string, 
  `analyst_cd` string, 
  `coll_cd` string, 
  `ref_no` string, 
  `ceris_no` string, 
  `user_id` string, 
  `cheque_no` string, 
  `expiry_dt` string, 
  `bank_sort_cd` string, 
  `cheque_dt` string, 
  `fran_area` string)
PARTITIONED BY ( 
  `dt` string, 
  `entity` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://s3cbrmddv02/converted/source/payment/daily_coll/daily_posted_payments'
TBLPROPERTIES (
  'parquet.compression'='SNAPPY')