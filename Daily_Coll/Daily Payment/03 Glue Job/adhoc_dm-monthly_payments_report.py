import sys
import time
import pandas as pd
import boto3
import datetime

from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3cbrmddv02"
        self.DATABASE_NAME = "cbrm_payment_daily"
        self.parameter = "2023-02-01"

    def run_athena_script(self):
        """Run SQL script to populate target table with the latest subid faid pair hist"""
        print("--- Run SQL script to populate target table")

        athena_client = boto3.client("athena")

        config = {
            "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
        }

        # Retrieve the SQL script from the S3 bucket
        bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
        obj = bucket.Object("sql_script/payments/monthly_payments_report_parameterized.sql")
        script = obj.get()["Body"].read().decode("utf-8")

        # Split the script into separate statements
        statements = script.split(";")

        # Execute each statement individually
        for statement in statements:
            # Trim leading and trailing whitespaces
            statement = statement.strip()

            # Skip empty statements and comments
            if not statement or statement.startswith("--") or statement.startswith("/*"):
                continue

            # You can use placeholders in your SQL script and replace them with actual parameters
            # For example, if you have a placeholder "?", you can replace it with a parameter value
            sql = [statement.replace("?", f"'{self.parameter}'"), 
                  f"EXECUTE insert_monthly_payments_report_hist;"]

            context = {"Database": self.DATABASE_NAME}
            for sql_exec in sql:
                res = athena_client.start_query_execution(
                    QueryString=sql_exec,
                    QueryExecutionContext=context,
                    ResultConfiguration=config
                )

            print(res)

            query_execution_id = res["QueryExecutionId"]
            self.wait_for_query_completion(query_execution_id, athena_client)
            self.export_query_result_to_csv(query_execution_id, athena_client)

    def wait_for_query_completion(self, query_execution_id, athena_client):
        """Wait for the query execution to complete"""
        while True:
            response = athena_client.get_query_execution(QueryExecutionId=query_execution_id)
            state = response['QueryExecution']['Status']['State']
            if state in ['SUCCEEDED', 'FAILED', 'CANCELLED']:
                break
            time.sleep(5)

    def export_query_result_to_csv(self, query_execution_id, athena_client):
        """Export the query result to a Excel file"""
        # Get the query results metadata
        query_results = athena_client.get_query_results(QueryExecutionId=query_execution_id)

        # Extract column names from the results metadata
        column_names = [field['Name'] for field in query_results['ResultSet']['ResultSetMetadata']['ColumnInfo']]

        # Initialize a list to store the rows
        rows = []

        # Extract rows from the query results
        for row in query_results['ResultSet']['Rows']:
            rows.append([field.get('VarCharValue', '') for field in row['Data']])

        # Create a DataFrame from the rows and column names
        df = pd.DataFrame(rows, columns=column_names)

        # Define the output file path
        parameter_datetime = datetime.datetime.strptime(self.parameter, "%Y-%m-%d")
        output_file_path = f"s3://{self.BUCKET_NAME}/output/payments/monthly_payments_report_files/monthly_payments_report_{parameter_datetime.strftime('%Y%m%d')}.xlsx"

        # Save the DataFrame as a Excel file in S3
        df.to_csv(output_file_path, index=False)

        print(f"Excel file exported: {output_file_path}")

    def process(self):
        self.run_athena_script()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
