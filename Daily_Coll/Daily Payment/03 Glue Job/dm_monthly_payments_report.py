import boto3
import datetime
import pandas as pd
import pyarrow
import re
import sys


from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        # self.BUCKET_NAME = "s3cbrmdpd01"
        self.BUCKET_NAME = "s3cbrmddv02"
        self.DATABASE_NAME = "cbrm_payment_daily"

    def run_athena_script(self):
        """Run prepare statement to populate target table with latest subid faid pair hist"""
        print("--- Run PREPARE Statement insert_daily_payments_report_hist")

        athena_client = boto3.client("athena")

        config = {
            "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
            # "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
        }
        
        
        sql = [f"PREPARE monthly_daily_payments_report_hist FROM INSERT INTO daily_payments_report_hist SELECT * FROM view_daily_payments_report;",
               f"EXECUTE monthly_daily_payments_report_hist;"] #using {datetime.datetime.now().strftime('%Y-%m-%d')};
        
        
        context = {"Database": self.DATABASE_NAME}
        for sql_exec in sql:
            res = athena_client.start_query_execution(
                QueryString=sql_exec,
                QueryExecutionContext=context,
                ResultConfiguration=config,
            )
        print(res)

        status = athena_client.get_query_execution(
            QueryExecutionId=res["QueryExecutionId"]
        )
        print(status)

    def process(self):
        self.run_athena_script()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
