import boto3
import datetime
import pandas as pd
import pyarrow
import re
import sys


from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

# from zoneinfo import ZoneInfo


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3cbrmddv02"
        #self.BUCKET_NAME = "s3cbrmdpd01-non-adh"
        self.DATABASE_NAME = "cbrm_payment_daily"
        #self.DATABASE_NAME = "cbrm_prd_datamart"

    def copy_csv_(self):
        """Copy files from csv"""
        print("--- Copy files from csv")
        regex_ymd = "\d{4}\d{2}"
        regex_filename = "\/(?:.(?!\/))+$"
        prefix_list = ["source/lookup/wireline_aging/"]

        for prefix in prefix_list:
            response = self.s3_client.list_objects_v2(
                Bucket=self.BUCKET_NAME,
                Prefix=prefix,
            )
            print(response)
            for i in response["Contents"]:
                if i["Key"][::-1][0] == "/":
                    continue

                obj_key = i["Key"]
                print(obj_key)
                copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
                s3_bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
                obj = s3_bucket.Object(key=obj_key)
                print(obj)

                colnames = [
                    "cust_ac_no", 
                    "ac_stat", 
                    "cust_classn", 
                    "cust_classn_desc", 
                    "ac_cat", 
                    "ac_cat_desc", 
                    "last_name", 
                    "first_name", 
                    "fran_area", 
                    "os_balance", 
                    "credits", 
                    "olt30days", 
                    "o30to60days", 
                    "o60to90days", 
                    "o90to120days", 
                    "o120to150days", 
                    "o150to180days", 
                    "o180to210days", 
                    "ogt210days", 
                    "currency_cd", 
                    "age", 
                    "service", 
                    "acctg_grp_old", 
                    "classification_old", 
                    "acctg_grp", 
                    "classification", 
                    "inst_st_dt", 
                    "salesman_id", 
                    "postal_cd", 
                    "province", 
                    "bill_postal_cd", 
                    "tenure", 
                    "region", 
                    "prod_id", 
                    "prod_name", 
                    "svc_type", 
                    "package", 
                    "product", 
                    "tenure_group",
                ]
                
                print('load body to var')
                # filepath = obj.get()["Body"]
                self.s3_client.download_file(self.BUCKET_NAME, obj_key, 'source')
                print('read csv file')
            
                df = pd.read_csv(
                    # filepath,
                    'source',
                    names=colnames,
                    header=0,
                    # escapechar="\\", # issues with columns whose value is \
                    quotechar='"',
                    sep=",",
                    dtype=str,
                    na_filter=False,
                    encoding="latin1"
                )
                print(df)
                
                
                parquet_file = "foo.gz"
                df.to_parquet(parquet_file, compression="snappy")
                partition = re.search(regex_ymd, obj_key)
                if partition is None:
                    # TODO
                    print("<filename> has no matching yyyymm string")
                    continue
                else:
                    partition = f"dt={partition.group()}"

                filename = re.search(regex_filename, obj_key)
                if filename is None:
                    # TODO
                    continue
                else:
                    filename = filename.group()
                    s3_path = obj_key[: -(len(filename))]

                print("---- converted")
                print("parquet_file: ", parquet_file)
                print("s3_path: ", s3_path)
                print("partition: ", partition)
                print("filename: ", filename)
                #print(f"converted/{s3_path}/{partition}{filename.replace('csv','gz')}")
                output_path = f"converted/source/lookup/wireline_aging/{partition}{filename.replace('csv','gz')}"
                self.s3_client.upload_file(
                    parquet_file,
                    self.BUCKET_NAME,
                    output_path,
                )


                #TODO move and/or delete
                #3 month retention period for the processed directory
                self.s3_client.copy_object(
                    Bucket=self.BUCKET_NAME,
                    CopySource=copy_src,
                    Key=f"processed/{s3_path}{filename}",
                )
                #self.s3_client.delete_object(Bucket=self.BUCKET_NAME, Key=obj_key)


    def load_partitions(self):
        """load new Hive partitions into a partitioned table"""
        print("--- Load partitions")
        views = ["wireline_aging", "exception_wireline_prq"]
        for view in views:
            athena_client = boto3.client("athena")

            config = {
                "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
                "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
            }
            sql = f"MSCK REPAIR TABLE {self.DATABASE_NAME}.{view}"
            context = {"Database": self.DATABASE_NAME}
            athena_client.start_query_execution(
                QueryString=sql,
                QueryExecutionContext=context,
                ResultConfiguration=config,
            )

    def get_athena_output(self):
        views = [
            {
                "view": "view_wireline_aging",
                "output_prefix": "output/daily_collections/wireline_aging",
                "name": "wireline_aging",
            }
        ]
        for i in views:
            print("--- Get Athena View")
            print(f"s3://{self.BUCKET_NAME}/{i['output_prefix']}")
            athena_view_dataframe = (
                self.glueContext.read.format("jdbc")
                .option("driver", "com.simba.athena.jdbc.Driver")
                .option(
                    "AwsCredentialsProviderClass",
                    "com.simba.athena.amazonaws.auth.InstanceProfileCredentialsProvider",
                )
                .option(
                    "url", "jdbc:awsathena://athena.ap-southeast-1.amazonaws.com:443"
                )
                .option(f"dbtable", f"AwsDataCatalog.{self.DATABASE_NAME}.{i['view']}")
                .option(
                    "S3OutputLocation",
                    "s3://aws-glue-assets-472359187062-ap-southeast-1/temporary/",
                )
                .load()
            )

            print(athena_view_dataframe)
            athena_view_dataframe.printSchema()
            print("view count: ", athena_view_dataframe.count())

            athena_view_datasource = DynamicFrame.fromDF(
                athena_view_dataframe, self.glueContext, "athena_view_source"
            )

            vw_output = self.glueContext.write_dynamic_frame.from_options(
                frame=athena_view_datasource,
                connection_type="s3",
                format="csv",
                connection_options={
                    "path": f"s3://{self.BUCKET_NAME}/{i['output_prefix']}/",
                    "partitionKeys": [],
                },
                # format_options={"compression": "snappy"},
                transformation_ctx="vw_output",
            )

            self.rename_athena_output(i["output_prefix"], i["name"])

    def rename_athena_output(self, prefix, name):
        print("--- Rename Athena output")

        response = self.s3_client.list_objects_v2(
            Bucket=self.BUCKET_NAME,
            Prefix=prefix + "/run-",
        )
        print(response)
        for i in response["Contents"]:
            print("content key: ", i)
            if i["Key"][::-1][0] == "/":
                continue
            copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
            datetime_now = (
                # datetime.datetime.now(ZoneInfo("Asia/Manila"))
                datetime.datetime.now()
                .replace(microsecond=0)
                .isoformat()
                .replace(":", "")
                .replace("-", "")
            )
            key = f"{prefix}/{name}_{datetime_now}.csv"
            print("copy_src: ", copy_src)
            print("key: ", key)
            self.s3_client.copy_object(
                Bucket=self.BUCKET_NAME, CopySource=copy_src, Key=key
            )
            self.s3_client.delete_object(Bucket=self.BUCKET_NAME, Key=i["Key"])

    def process(self):
        self.copy_csv_()
        self.load_partitions()
        #self.get_athena_output()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
