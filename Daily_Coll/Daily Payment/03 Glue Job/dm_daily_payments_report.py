import boto3
import datetime
import pandas as pd
import pyarrow
import re
import sys


from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        # self.BUCKET_NAME = "s3cbrmdpd01"
        self.BUCKET_NAME = "s3cbrmddv02"
        self.DATABASE_NAME = "cbrm_payment_daily"

    def run_athena_script(self):
        """Run prepare statement to populate target table with latest subid faid pair hist"""
        print("--- Run PREPARE Statement insert_daily_payments_report_hist")

        athena_client = boto3.client("athena")

        config = {
            "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
            # "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
        }
        
        
        sql = [f"PREPARE insert_daily_payments_report_hist FROM INSERT INTO daily_payments_report_hist SELECT * FROM view_daily_payment_reports;",
               f"EXECUTE insert_daily_payments_report_hist;"] #using {datetime.datetime.now().strftime('%Y-%m-%d')};
        
        
        context = {"Database": self.DATABASE_NAME}
        for sql_exec in sql:
            res = athena_client.start_query_execution(
                QueryString=sql_exec,
                QueryExecutionContext=context,
                ResultConfiguration=config,
            )
        print(res)

        status = athena_client.get_query_execution(
            QueryExecutionId=res["QueryExecutionId"]
        )
        print(status)
        
        
    def get_athena_output(self):
        views = [
            {
                "view": "view_daily_payments_reports_20230101",
                "output_prefix": "output/payments/daily_payments_report_files",
                "name": "daily_payments_report",
            }
        ]
        for i in views:
            print("--- Get Athena View")
            print(f"s3://{self.BUCKET_NAME}/{i['output_prefix']}")
            athena_view_dataframe = (
                self.glueContext.read.format("jdbc")
                .option("driver", "com.simba.athena.jdbc.Driver")
                .option(
                    "AwsCredentialsProviderClass",
                    "com.simba.athena.amazonaws.auth.InstanceProfileCredentialsProvider",
                )
                .option(
                    "url", "jdbc:awsathena://athena.ap-southeast-1.amazonaws.com:443"
                )
                .option(f"dbtable", f"AwsDataCatalog.{self.DATABASE_NAME}.{i['view']}")
                .option(
                    "S3OutputLocation",
                    "s3://aws-glue-assets-717910298350-ap-southeast-1/temporary/"
                    #"s3://aws-glue-assets-472359187062-ap-southeast-1/temporary/",
                )
                .load()
            )

            print(athena_view_dataframe)
            athena_view_dataframe.printSchema()
            print("view count: ", athena_view_dataframe.count())

            athena_view_datasource = DynamicFrame.fromDF(
                athena_view_dataframe, self.glueContext, "athena_view_source"
            )

            vw_output = self.glueContext.write_dynamic_frame.from_options(
                frame=athena_view_datasource,
                connection_type="s3",
                format="csv",
                connection_options={
                    "path": f"s3://{self.BUCKET_NAME}/{i['output_prefix']}/",
                    "partitionKeys": [],
                },
                # format_options={"compression": "snappy"},
                transformation_ctx="vw_output",
            )

            self.rename_athena_output(i["output_prefix"], i["name"])

    def rename_athena_output(self, prefix, name):
        print("--- Rename Athena output")

        response = self.s3_client.list_objects_v2(
            Bucket=self.BUCKET_NAME,
            Prefix=prefix + "/run-",
        )
        print(response)
        for i in response["Contents"]:
            print("content key: ", i)
            if i["Key"][::-1][0] == "/":
                continue
            copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
            datetime_now = datetime.datetime.now().strftime("%m%d%Y")
            key = f"{prefix}/{name}_{datetime_now}.xlsx"
            print("copy_src: ", copy_src)
            print("key: ", key)
            self.s3_client.copy_object(
                Bucket=self.BUCKET_NAME, CopySource=copy_src, Key=key
            )
            self.s3_client.delete_object(Bucket=self.BUCKET_NAME, Key=i["Key"])

    def process(self):
        self.run_athena_script()
        self.get_athena_output()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
