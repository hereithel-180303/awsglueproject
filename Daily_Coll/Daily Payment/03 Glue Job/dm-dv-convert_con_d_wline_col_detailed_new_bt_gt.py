import boto3
import datetime
import pandas as pd
import pyarrow
import re
import sys


from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

# from zoneinfo import ZoneInfo


class GlueJob:
    def __init__(self):
        args = getResolvedOptions(sys.argv, ["JOB_NAME"])
        print(args)
        sc = SparkContext()
        self.glueContext = GlueContext(sc)
        self.spark = self.glueContext.spark_session
        self.job = Job(self.glueContext)
        self.job.init(args["JOB_NAME"], args)

        self.s3_client = boto3.client("s3")
        self.s3_resource = boto3.resource("s3")

        self.BUCKET_NAME = "s3cbrmddv02"
        #self.BUCKET_NAME = "s3cbrmdpd01-non-adh"
        self.DATABASE_NAME = "cbrm_payment_daily"
        #self.DATABASE_NAME = "cbrm_prd_datamart"

    def csv_to_parquet(self):
        """convert csv files to parquet files"""
        print("--- Convert files from csv to parquet")
        regex_ymd = "\d{4}\d{2}\d{2}"
        regex_filename = "\/(?:.(?!\/))+$"
        
        """Dev S3"""
        prefix_list = [
            "source/Daily_Coll/Interim/con_d_wline_col_detailed_new-GT",
            "source/Daily_Coll/Interim/con_d_wline_col_detailed_new-BT",
        ]
    
        """Prod S3"""
        #prefix_list = [
        #    "source/payment/daily_coll/con_d_wline_col_detailed_new-BT/",
        #    "source/payment/daily_coll/con_d_wline_col_detailed_new-GT/",
        #]
        
        
        for prefix in prefix_list:
            response = self.s3_client.list_objects_v2(
                Bucket=self.BUCKET_NAME,
                Prefix=prefix,
            )
            print(response)
            for i in response["Contents"]:
                if i["Key"][::-1][0] == "/":
                    continue

                obj_key = i["Key"]
                print(obj_key)
                copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
                s3_bucket = self.s3_resource.Bucket(self.BUCKET_NAME)
                obj = s3_bucket.Object(key=obj_key)
                print(obj)

                colnames = [
                    "cust_ac_no",
                    "receipt_dt",
                    "cashbox_dt",
                    "receipt_amt",
                    "ac_cat",
                    "cust_classn",
                    "ac_stat",
                    "ac_cat_desc",
                    "cust_classn_desc",
                    "fran_area",
                ]

                df = pd.read_csv(
                    obj.get()["Body"],
                    names=colnames,
                    header=0,
                    # escapechar="\\", # issues with columns whose value is \
                    quotechar='"',
                    sep=",",
                    dtype=str,
                    na_filter=False,
                )
                print(df)
                parquet_file = "foo.gz"
                df.to_parquet(parquet_file, compression="snappy")
                partition = re.search(regex_ymd, obj_key)
                if partition is None:
                    # TODO
                    print("<filename> has no matching yyyymmdd string")
                    continue
                else:
                    partition = f"dt={partition.group()}"

                filename = re.search(regex_filename, obj_key)
                if filename is None:
                    # TODO
                    continue
                else:
                    filename = filename.group()
                    s3_path = obj_key[: -(len(filename))]

                if "_BT" in filename:
                    entity = "entity=Bayan"
                elif "_GT" in filename:
                    entity = "entity=Innove"
                else:
                    entity = "entity=unknown"

                print("---- converted")
                print("parquet_file: ", parquet_file)
                print("s3_path: ", s3_path)
                print("partition: ", partition)
                print("filename: ", filename)
                output_path = f"converted/source/payment/daily_coll/con_d_wline_col_detailed_new/{partition}/{entity}{filename.replace('csv','gz')}"
                print(output_path)
                self.s3_client.upload_file(
                    parquet_file,
                    self.BUCKET_NAME,
                    output_path,
                )
                # TODO move and/or delete
                self.s3_client.copy_object(
                    Bucket=self.BUCKET_NAME,
                    CopySource=copy_src,
                    Key=f"processed/{s3_path}{filename}",
                )
                # self.s3_client.delete_object(Bucket=self.BUCKET_NAME, Key=obj_key)

    def load_partitions(self):
        """load new Hive partitions into a partitioned table"""
        print("--- Load partitions")
        views = [
            "con_d_wline_col_detailed_new_bt",
            "con_d_wline_col_detailed_new_gt",    
                ]
        for view in views:
            athena_client = boto3.client("athena")

            config = {
                "OutputLocation": f"s3://{self.BUCKET_NAME}/output/temp/",
                "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
            }
            sql = f"MSCK REPAIR TABLE {self.DATABASE_NAME}.{view}"
            context = {"Database": self.DATABASE_NAME}
            athena_client.start_query_execution(
                QueryString=sql,
                QueryExecutionContext=context,
                ResultConfiguration=config,
            )

    '''def get_athena_output(self):
        views = [
            {
                "view": "view_339__detail_payments_report_prq",
                "output_prefix": "output/daily_collections/339. Detail Payments Report",
                "name": "detail_payments_report",
            }
        ]
        for i in views:
            print("--- Get Athena View")
            print(f"s3://{self.BUCKET_NAME}/{i['output_prefix']}")
            athena_view_dataframe = (
                self.glueContext.read.format("jdbc")
                .option("driver", "com.simba.athena.jdbc.Driver")
                .option(
                    "AwsCredentialsProviderClass",
                    "com.simba.athena.amazonaws.auth.InstanceProfileCredentialsProvider",
                )
                .option(
                    "url", "jdbc:awsathena://athena.ap-southeast-1.amazonaws.com:443"
                )
                .option(f"dbtable", f"AwsDataCatalog.{self.DATABASE_NAME}.{i['view']}")
                .option(
                    "S3OutputLocation",
                    "s3://aws-glue-assets-717910298350-ap-southeast-1/temporary/",
                )
                .load()
            )

            print(athena_view_dataframe)
            athena_view_dataframe.printSchema()
            print("view count: ", athena_view_dataframe.count())

            athena_view_datasource = DynamicFrame.fromDF(
                athena_view_dataframe, self.glueContext, "athena_view_source"
            )

            # TODO Jayson add loop by 3000 from athena_view_datasource (split source into 3000 rows)
            # pseudo for i in (df, 3000): # don't forget to indent code below to save and rename the files.
            vw_output = self.glueContext.write_dynamic_frame.from_options(
                frame=athena_view_datasource,
                connection_type="s3",
                format="csv",
                connection_options={
                    "path": f"s3://{self.BUCKET_NAME}/{i['output_prefix']}/",
                    "partitionKeys": [],
                },
                # format_options={"compression": "snappy"},
                transformation_ctx="vw_output",
            )

            self.rename_athena_output(i["output_prefix"], i["name"])

    def rename_athena_output(self, prefix, name):
        print("--- Rename Athena output")

        response = self.s3_client.list_objects_v2(
            Bucket=self.BUCKET_NAME,
            Prefix=prefix + "/run-",
        )
        print(response)
        for i in response["Contents"]:
            print("content key: ", i)
            if i["Key"][::-1][0] == "/":
                continue
            copy_src = f"{self.BUCKET_NAME}/{i['Key']}"
            datetime_now = (
                # datetime.datetime.now(ZoneInfo("Asia/Manila"))
                datetime.datetime.now()
                .replace(microsecond=0)
                .isoformat()
                .replace(":", "")
                .replace("-", "")
            )
            key = f"{prefix}/{name}_{datetime_now}.csv"
            print("copy_src: ", copy_src)
            print("key: ", key)
            self.s3_client.copy_object(
                Bucket=self.BUCKET_NAME, CopySource=copy_src, Key=key
            )
            self.s3_client.delete_object(Bucket=self.BUCKET_NAME, Key=i["Key"])'''

    def process(self):
        self.csv_to_parquet()
        self.load_partitions()
        # self.get_athena_output()
        self.job.commit()


glue_job = GlueJob()
glue_job.process()
