--CREATE OR REPLACE VIEW "view_daily_payments_report" AS 
with current_query as 
	( -- 339 VIEW DETAIL PAYMENTS REPORT, DETAIL PAYMENTS REPORT WLN_B, DETAIL PAYMENTS REPORT WLN_I
		select f.*
			from( 
					select
						file339.filename,
						file339.payment_id Payment_ID,
						file339.Financial_Account_ID,
						file339.status,
						file339.Entity,
						'MyBSS' Source,
						CASE 
							WHEN (a_wireless_l.class = 'SMB-SME') THEN 'SG' 
							WHEN (a_wireless_l.class = 'Consumer') THEN 'Consumer' 
							WHEN (a_wireless_l.class in ('Enterprise', 'SMB-Corporate')) THEN 'EG' 
							ELSE a_wireless_l.class
						END CFU,
						date_parse(file339.posting_date, '%Y%m%d') as Posting_Date,
						cast(file339.receipt_amount as decimal(38,2)) Payt,
						file339.gbu_name,
						file339.gbu_name_final
					from (
						-- Globe
						select
							*
							--'Globe' Entity
						from (
							select
								v_dpr.*,
								upper(cml.final_account_name) as gbu_name, -- for SG only
								upper(v_gbu_w_eg.gbu_name_final) as gbu_name_final
							from view_339__detail_payments_report v_dpr
							left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
								on v_dpr.Financial_Account_ID = cml.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m' ))	
							left join view_gbu_wless_eg v_gbu_w_eg
								on v_dpr.Financial_Account_ID = v_gbu_w_eg.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m' ))
							where v_dpr.receipt_amount < 30000000
						) as a
						union all -- Bayan
						select
							*
							--'Bayan' Entity
						from (
							select
								v_dpr_b.*,
								upper(cml.final_account_name) as gbu_name, -- for SG only
								upper(v_gbu_w_eg.gbu_name_final) as gbu_name_final
							from view_339__detail_payments_report___wln_b v_dpr_b
							left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
								on v_dpr_b.Financial_Account_ID = cml.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr_b.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
							left join view_gbu_wless_eg v_gbu_w_eg
								on v_dpr_b.Financial_Account_ID = v_gbu_w_eg.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr_b.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m'))
							where v_dpr_b.receipt_amount < 10000000
						) as b
						union all -- Innove
						select
							*
							--'Innove' Entity
						from (
							select
								v_dpr_i.*,
								upper(cml.final_account_name) as gbu_name, -- for SG only
								upper(v_gbu_w_eg.companyname) as gbu_name_final
							from view_339__detail_payments_report___wln_i v_dpr_i
							left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
								on v_dpr_i.Financial_Account_ID = cml.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr_i.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
							left join view_gbu_wline_eg v_gbu_w_eg
								on v_dpr_i.Financial_Account_ID = v_gbu_w_eg.financial_account_id
								and ((date_trunc('month', date_parse(v_dpr_i.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m'))
							where v_dpr_i.receipt_amount < 10000000
						) as c
					)
					as file339
					left join view_mybss_lookup a_wireless_l 
						on ((file339.account_type = a_wireless_l.customer_type)
						and (file339.account_sub_type = a_wireless_l.customer_sub_type))
					where date_parse("regexp_extract"(file339.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') = current_date	
						and a_wireless_l.class != 'INHOUSE' 
						and trim(concat(file339.account_type,file339.account_sub_type)) != 'CS'		
				) f
			where f.cfu in ('Consumer','EG', 'SG')
		UNION ALL -- DAILY POSTED PAYMENT BT
		select 
			s.* 
		from (
		select 
			d_p_payment_bt.filename,
			d_p_payment_bt.Payment_ID,
			d_p_payment_bt.Financial_Account_ID,
			d_p_payment_bt.status,
			'Bayan' Entity,
			'ICCBS' Source,
			CASE
				WHEN d_p_payment_bt.cust_classn = 'C' and d_p_payment_bt.ac_cat = 'M'  THEN 'EG'
				WHEN d_p_payment_bt.classification in ('SME','BUSINESS') THEN  'SG' 
				WHEN d_p_payment_bt.classification = 'CORPORATE' THEN 'EG'
				WHEN d_p_payment_bt.classification in ('ENTERPRISE','OSS','CONVERGENCE')  THEN 'EG'
				ELSE d_p_payment_bt.classification
			END as CFU,		
			d_p_payment_bt.Posting_Date,
			cast(d_p_payment_bt.Payt as decimal(38,2)) Payt,
			d_p_payment_bt.gbu_name,
			d_p_payment_bt.gbu_name_final
			from (
					select 
						DISTINCT
						v_dpp_bt.filename,
						v_dpp_bt.Payment_ID,
						v_dpp_bt.Financial_Account_ID,
						v_dpp_bt.cust_name,
						v_dpp_bt.ac_stat status,
						v_b_lookup.cust_classn,
						v_b_lookup.ac_cat,
						v_b_lookup.service,
						v_b_lookup.classification,
						date_parse(v_dpp_bt.post_date, '%Y%m%d') Posting_Date,
						sum(case 
							when v_dpp_bt.actual_payment_currency = 'USD' then v_dpp_bt.receipt_amt 
							else v_dpp_bt.actual_amt_received
						end) as Payt,
						upper(gbu_b_eg.companyname) as gbu_name_final,
					    upper(cml.final_account_name) as gbu_name  -- for SG only
					from view_daily_posted_payments_bt v_dpp_bt
					left join view_bayan_lookup v_b_lookup
						on v_dpp_bt.Financial_Account_ID = v_b_lookup.financial_account_id
						and ((date_trunc('month', date_parse(v_dpp_bt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_b_lookup.filename, '\d{4}\d{2}'), '%Y%m'))
					left join view_iccbs_lookup a_wireline_l
						on v_b_lookup.ac_cat = a_wireline_l.ac_cat
						and v_b_lookup.cust_classn = a_wireline_l.cust_classn
					left join view_gbu_bayan_eg gbu_b_eg
						on v_dpp_bt.Financial_Account_ID = gbu_b_eg.financial_account_id
						and ((date_trunc('month', date_parse(v_dpp_bt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(gbu_b_eg.filename, '\d{4}\d{2}'), '%Y%m'))
					left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
						on v_dpp_bt.Financial_Account_ID = cml.financial_account_id
						and ((date_trunc('month', date_parse(v_dpp_bt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
					where 
						trim(concat(substr(v_b_lookup.cust_classn, + 1, 1),substr(v_b_lookup.ac_cat, + 1, 1))) != 'CP'
						-- and v_dpp_bt.Financial_Account_ID != ' '
						and v_dpp_bt.cust_name not like '%ffected)'	
					GROUP by v_dpp_bt.filename, 
							v_dpp_bt.Payment_ID, 
							v_dpp_bt.Financial_Account_ID,
							v_dpp_bt.cust_name,
							v_dpp_bt.ac_stat,
							v_b_lookup.cust_classn,
							v_b_lookup.ac_cat,
							v_b_lookup.service,
							v_b_lookup.classification,
							v_dpp_bt.post_date,
							v_dpp_bt.receipt_amt,
							gbu_b_eg.companyname,
							cml.final_account_name
			) d_p_payment_bt
			where d_p_payment_bt.service in ('DATA-PHP', 'DATA-USD')
				and d_p_payment_bt.cust_name not like 'GLOBE%'
				and d_p_payment_bt.cust_name not like 'GT %'
				and d_p_payment_bt.cust_name not like 'INNOVE%'
				and d_p_payment_bt.Posting_Date = current_date - interval '1' day
				and date_parse("regexp_extract"(d_p_payment_bt.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') =  current_date - interval '1' day
		) as s
		where s.cfu IN ('EG', 'SG')	
		UNION ALL -- DAILY POSTED PAYMENT GT
		select 
			j.* 
		from (
			select 
				d_p_payment_gt.filename,
				d_p_payment_gt.Payment_ID,
				d_p_payment_gt.Financial_Account_ID,
				d_p_payment_gt.status,
				'Innove' Entity,
				'ICCBS' Source,
				CASE
					WHEN d_p_payment_gt.cust_classn = 'C' and d_p_payment_gt.ac_cat = 'M' THEN 'EG'
					WHEN d_p_payment_gt.classification in ('SME','BUSINESS')  THEN  'SG'
					WHEN d_p_payment_gt.classification = 'CORPORATE' THEN 'EG' 
					WHEN d_p_payment_gt.classification in ('ENTERPRISE','OSS','CONVERGENCE') THEN 'EG'
					ELSE d_p_payment_gt.classification
				END as CFU,		
				d_p_payment_gt.Posting_Date,
				cast(d_p_payment_gt.Payt as decimal(38,2)) Payt,
				d_p_payment_gt.gbu_name,
				d_p_payment_gt.gbu_name_final
				from (
						select 
							DISTINCT
							v_dpp_gt.filename,
							v_dpp_gt.Payment_ID,
							v_dpp_gt.Financial_Account_ID,
							v_dpp_gt.cust_name,
							v_dpp_gt.ac_stat status,
							v_w_aging.cust_classn,
							v_w_aging.ac_cat,
							v_w_aging.service,
							v_w_aging.classification,
							date_parse(v_dpp_gt.post_date, '%Y%m%d') Posting_Date,
							sum(case 
								when v_dpp_gt.actual_payment_currency = 'USD' then v_dpp_gt.receipt_amt
								else v_dpp_gt.actual_amt_received 
							end) as Payt,
							upper(cml.final_account_name) as gbu_name,  -- for SG only
					        upper(v_gbu_w_eg.companyname) as gbu_name_final
						from view_daily_posted_payments_gt v_dpp_gt
						left join view_wireline_aging v_w_aging
							on v_dpp_gt.Financial_Account_ID = v_w_aging.financial_account_id
							and ((date_trunc('month', date_parse(v_dpp_gt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_w_aging.filename, '\d{4}\d{2}'), '%Y%m'))
						left join view_iccbs_lookup a_wireline_l
							on v_w_aging.ac_cat = a_wireline_l.ac_cat
							and v_w_aging.cust_classn = a_wireline_l.cust_classn
						left join view_gbu_wline_eg v_gbu_w_eg			
							on v_dpp_gt.Financial_Account_ID = v_gbu_w_eg.financial_account_id
							and ((date_trunc('month', date_parse(v_dpp_gt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m'))
						left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
							on v_dpp_gt.Financial_Account_ID = cml.financial_account_id
							and ((date_trunc('month', date_parse(v_dpp_gt.post_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
						where 
							trim(concat(substr(v_w_aging.cust_classn, + 1, 1),substr(v_w_aging.ac_cat, + 1, 1))) != 'CP'
							-- and v_dpp_gt.Financial_Account_ID != ' '
							and v_dpp_gt.cust_name not like '%ffected)'
						GROUP By v_dpp_gt.filename, 
							v_dpp_gt.Payment_ID, 
							v_dpp_gt.Financial_Account_ID,
							v_dpp_gt.cust_name,
							v_dpp_gt.ac_stat,
							v_w_aging.cust_classn,
							v_w_aging.ac_cat,
							v_w_aging.service,
							v_w_aging.classification,
							v_dpp_gt.post_date,
							v_dpp_gt.receipt_amt,
							cml.final_account_name,
							v_gbu_w_eg.companyname
				) d_p_payment_gt
			where  d_p_payment_gt.service in ('DATA-PHP', 'DATA-USD') 
				and d_p_payment_gt.cust_name not like 'GLOBE%'
				and d_p_payment_gt.cust_name not like 'GT %'
				and d_p_payment_gt.cust_name not like 'INNOVE%'
				and d_p_payment_gt.Posting_Date = current_date - interval '1' day
				and date_parse("regexp_extract"(d_p_payment_gt.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') = current_date - interval '1' day
		) as j
		where j.cfu IN ('EG', 'SG')
		UNION ALL -- VIEW CON D WLINE  DETAILED NEW BT
		select 
			z.* 
		from (
			select
				con_wireline_d_bt.filename,
				con_wireline_d_bt.Payment_ID,
				con_wireline_d_bt.Financial_Account_ID,
				con_wireline_d_bt.status,
				'Bayan' Entity,
				'ICCBS' Source,
				CASE
					WHEN (con_wireline_d_bt.status = 'C' and con_wireline_d_bt.classification = 'RESIDENTIAL') THEN 'Consumer'
					WHEN (con_wireline_d_bt.status = 'C' and con_wireline_d_bt.classification in ('SME', 'BUSINESS') and con_wireline_d_bt.service = 'VOICE')  THEN 'SG'
					WHEN (con_wireline_d_bt.status = 'H' and con_wireline_d_bt.classification in ('SME', 'BUSINESS'))  THEN 'SG'
					WHEN (con_wireline_d_bt.status = 'C' and con_wireline_d_bt.classification in ('ENTERPRISE', 'CORPORATE') and con_wireline_d_bt.service = 'VOICE') THEN 'EG'
					WHEN (con_wireline_d_bt.status = 'H' and con_wireline_d_bt.classification in ('RESIDENTIAL', 'WRITE-OFF'))  THEN 'Consumer'
					ELSE con_wireline_d_bt.classification
				END as CFU,
				con_wireline_d_bt.Posting_Date,
				cast(con_wireline_d_bt.Payt as decimal(38,2)) Payt,
				con_wireline_d_bt.gbu_name,
				con_wireline_d_bt.gbu_name_final
			from (
				select
					DISTINCT
					d_wline_new_bt.filename,
					d_wline_new_bt.Payment_ID,
					d_wline_new_bt.Financial_Account_ID,
					d_wline_new_bt.ac_stat status,
					d_wline_new_bt.ac_cat,
					date_parse(d_wline_new_bt.cashbox_dt, '%Y%m%d') as Posting_Date,
					sum(d_wline_new_bt.receipt_amt) Payt,
					d_wline_new_bt.cust_classn,
					v_b_lookup.service,
					v_b_lookup.classification,
					upper(gbu_b_eg.companyname) as gbu_name_final,
					upper(cml.final_account_name) as gbu_name  -- for SG only
				from view_con_d_wline_col_detailed_new_bt d_wline_new_bt
				left join view_bayan_lookup v_b_lookup
					on d_wline_new_bt.Financial_Account_ID = v_b_lookup.financial_account_id 
					and ((date_trunc('month', date_parse(d_wline_new_bt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_b_lookup.filename, '\d{4}\d{2}'), '%Y%m'))
				left join view_iccbs_lookup a_wireline_l  
					on d_wline_new_bt.ac_cat = a_wireline_l.ac_cat  
					and d_wline_new_bt.cust_classn = a_wireline_l.cust_classn
				left join view_gbu_bayan_eg gbu_b_eg
					on d_wline_new_bt.Financial_Account_ID = gbu_b_eg.financial_account_id
					and ((date_trunc('month', date_parse(d_wline_new_bt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(gbu_b_eg.filename, '\d{4}\d{2}'), '%Y%m'))
				left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
					on d_wline_new_bt.Financial_Account_ID = cml.financial_account_id
					and ((date_trunc('month', date_parse(d_wline_new_bt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
				where v_b_lookup.ac_cat not in ('X', 'H')
				GROUP by d_wline_new_bt.filename, 
							d_wline_new_bt.Payment_ID, 
							d_wline_new_bt.Financial_Account_ID,
							d_wline_new_bt.ac_stat,
							d_wline_new_bt.ac_cat,
							d_wline_new_bt.cashbox_dt,
							d_wline_new_bt.receipt_amt,
							d_wline_new_bt.cust_classn,
							v_b_lookup.service,
							v_b_lookup.classification,
							gbu_b_eg.companyname,
							cml.final_account_name
			) as con_wireline_d_bt
			where 
				con_wireline_d_bt.Posting_Date = current_date - interval '1' day
				and date_parse("regexp_extract"(con_wireline_d_bt.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') =  current_date - interval '1' day
			having con_wireline_d_bt.classification in (
					'CORPORATE',
					'SME',
					'ENTERPRISE',
					'RESIDENTIAL',
					'WRITE-OFF',
					'BUSINESS'
				)
		) as z 
		where z.cfu IN ('Consumer', 'EG', 'SG')	
		UNION ALL -- VIEW CON D WLINE  DETAILED NEW GT 
		select 
			q.*
		from (
			select
				con_wireline_d_gt.filename,
				con_wireline_d_gt.Payment_ID,
				con_wireline_d_gt.Financial_Account_ID,
				con_wireline_d_gt.status,
				'Innove' Entity,
				'ICCBS' Source,
				CASE
					WHEN con_wireline_d_gt.status = 'C' and con_wireline_d_gt.classification = 'RESIDENTIAL' THEN 'Consumer'
					WHEN con_wireline_d_gt.status = 'C' and con_wireline_d_gt.classification = 'SME' and con_wireline_d_gt.service = 'VOICE' THEN 'SG'
					WHEN con_wireline_d_gt.status = 'H' and con_wireline_d_gt.classification = 'SME' THEN 'SG'	
					WHEN con_wireline_d_gt.status = 'C' and con_wireline_d_gt.classification in ('ENTERPRISE', 'CORPORATE') and con_wireline_d_gt.service = 'VOICE' THEN 'EG'
					WHEN con_wireline_d_gt.status = 'H' and con_wireline_d_gt.classification in ('RESIDENTIAL', 'WRITE-OFF') THEN 'Consumer'
					ELSE con_wireline_d_gt.classification
				END as CFU,
				con_wireline_d_gt.Posting_Date,
				cast(con_wireline_d_gt.Payt as decimal(38,2)) Payt,
				con_wireline_d_gt.gbu_name,
				con_wireline_d_gt.gbu_name_final
			from (
				select
					DISTINCT
					d_wline_new_gt.filename,
					d_wline_new_gt.Payment_ID,
					d_wline_new_gt.Financial_Account_ID,
					d_wline_new_gt.ac_stat status,
					d_wline_new_gt.ac_cat,
					date_parse(d_wline_new_gt.cashbox_dt, '%Y%m%d') as Posting_Date,
					sum(d_wline_new_gt.receipt_amt) Payt,
					d_wline_new_gt.cust_classn,
					v_w_aging.service,
					v_w_aging.classification,
					upper(cml.final_account_name) as gbu_name,  -- for SG only
					upper(v_gbu_w_eg.companyname) as gbu_name_final
				from view_con_d_wline_col_detailed_new_gt d_wline_new_gt
				left join view_wireline_aging v_w_aging	
					on d_wline_new_gt.Financial_Account_ID = v_w_aging.financial_account_id 
					and ((date_trunc('month', date_parse(d_wline_new_gt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_w_aging.filename, '\d{4}\d{2}'), '%Y%m'))
				left join view_iccbs_lookup a_wireline_l
					on d_wline_new_gt.ac_cat = a_wireline_l.ac_cat
					and d_wline_new_gt.cust_classn = a_wireline_l.cust_classn
				left join view_gbu_wline_eg v_gbu_w_eg			
					on d_wline_new_gt.Financial_Account_ID = v_gbu_w_eg.financial_account_id
					and ((date_trunc('month', date_parse(d_wline_new_gt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m'))
				left join (select distinct sub_cml.filename, sub_cml.financial_account_id, sub_cml.final_account_name from view_cml_line_level sub_cml) cml
					on d_wline_new_gt.Financial_Account_ID = cml.financial_account_id
					and ((date_trunc('month', date_parse(d_wline_new_gt.cashbox_dt, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(cml.filename, '\d{4}\d{2}'), '%Y%m'))
				where v_w_aging.ac_cat not in ('X', 'H')
				GROUP by d_wline_new_gt.filename, 
							d_wline_new_gt.Payment_ID, 
							d_wline_new_gt.Financial_Account_ID,
							d_wline_new_gt.ac_stat,
							d_wline_new_gt.ac_cat,
							d_wline_new_gt.cashbox_dt,
							d_wline_new_gt.receipt_amt,
							d_wline_new_gt.cust_classn,
							v_w_aging.service,
							v_w_aging.classification,
							v_gbu_w_eg.companyname,
							cml.final_account_name
			) as con_wireline_d_gt
			where 
				con_wireline_d_gt.Posting_Date = current_date - interval '1' day
				and date_parse("regexp_extract"(con_wireline_d_gt.filename, '([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))'), '%Y%m%d') = current_date - interval '1' day 
			having con_wireline_d_gt.classification in (
					'CORPORATE',
					'SME',
					'ENTERPRISE',
					'RESIDENTIAL',
					'WRITE-OFF'
			)
		) as q 
		where q.cfu IN ('Consumer', 'EG', 'SG')
		UNION ALL -- EG Historic --	
		select
			bl_rcpts_join.filename,
			bl_rcpts_join.Payment_ID,
			bl_rcpts_join.Financial_Account_ID,
			bl_rcpts_join.status,
			CASE
				WHEN bl_rcpts_join.company_id = 'BT' THEN 'Bayan'
				WHEN bl_rcpts_join.company_id = 'GT' THEN 'Innove'
				ELSE ''
			END Entity,
			'ICCBS' Source,
			'EG' CFU,
			bl_rcpts_join.Posting_Date,
			cast(Payt as decimal(38,2)) as Payt,
			bl_rcpts_join.bt_gbu_name as gbu_name,
			bl_rcpts_join.gt_gbu_name as gbu_name_final
		from (
			select
				DISTINCT
				bl_rcpts.filename,
				bl_rcpts.Financial_Account_ID,
				bl_rcpts.Payment_ID,
				bl_rcpts.ac_stat status,
				bl_rcpts.ac_cat,
				date_parse(bl_rcpts.posting_date, '%Y%m%d') as Posting_Date,
				sum(bl_rcpts.converted) Payt,
				bl_rcpts.cust_classn,
				CASE	
					WHEN bl_rcpts.Financial_Account_ID = v_w_aging.financial_account_id THEN v_w_aging.classification
					WHEN bl_rcpts.Financial_Account_ID = v_b_lookup.financial_account_id THEN v_b_lookup.classification
				END as classification,
				upper(v_gbu_b_eg.companyname) as bt_gbu_name,
				upper(v_gbu_w_eg.companyname) as gt_gbu_name,
				bl_rcpts.company_id
			from view_bl_rcpts bl_rcpts
			left join view_iccbs_lookup a_wireline_l
				on bl_rcpts.ac_cat = a_wireline_l.ac_cat
				and bl_rcpts.cust_classn = a_wireline_l.cust_classn
			left join view_wireline_aging v_w_aging  
				on bl_rcpts.Financial_Account_ID = v_w_aging.financial_account_id	
				and ((date_trunc('month', date_parse(bl_rcpts.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_w_aging.filename, '\d{4}\d{2}'), '%Y%m'))
			left join view_bayan_lookup v_b_lookup 
				on bl_rcpts.Financial_Account_ID = v_b_lookup.financial_account_id
				and ((date_trunc('month', date_parse(bl_rcpts.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_b_lookup.filename, '\d{4}\d{2}'), '%Y%m'))
			left join view_gbu_bayan_eg v_gbu_b_eg
				on bl_rcpts.Financial_Account_ID = v_gbu_b_eg.financial_account_id
				and ((date_trunc('month', date_parse(bl_rcpts.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_b_eg.filename, '\d{4}\d{2}'), '%Y%m'))
			left join view_gbu_wline_eg v_gbu_w_eg
				on bl_rcpts.Financial_Account_ID = v_gbu_w_eg.financial_account_id
				and ((date_trunc('month', date_parse(bl_rcpts.posting_date, '%Y%m%d')) - interval '1' month) = date_parse("regexp_extract"(v_gbu_w_eg.filename, '\d{4}\d{2}'), '%Y%m'))
			where bl_rcpts.company_id in ('GT', 'BT')
			group by 
				bl_rcpts.filename,
				bl_rcpts.Financial_Account_ID,
				bl_rcpts.Payment_ID,
				bl_rcpts.ac_stat,
				bl_rcpts.ac_cat,
				bl_rcpts.posting_date,
				bl_rcpts.receipt_amt,
				bl_rcpts.cust_classn,
				v_w_aging.financial_account_id,
				v_b_lookup.financial_account_id,
				v_w_aging.classification,
				v_b_lookup.classification,
				v_gbu_b_eg.companyname,
				v_gbu_w_eg.companyname,
				bl_rcpts.company_id
		) as bl_rcpts_join
		where 
			bl_rcpts_join.status = 'H'
			and bl_rcpts_join.classification in ('CORPORATE', 'ENTERPRISE')
			and bl_rcpts_join.Posting_Date = current_date - interval '1' day
			and date_parse("regexp_extract"(bl_rcpts_join.filename, '([12]\d{3}(0[1-9]|1[0-2]))'), '%Y%m') = date_trunc('month', current_date - interval '1' month)
	)
select
	agg.Financial_Account_ID,
	agg.CFU,
	agg.Status,
	agg.Entity,
	agg.Source,
	date_format(agg.Posting_Date, '%Y%m%d') Posting_Date,
	cast(sum(Payt) as decimal(38,2)) as Payt,
	agg.GBU_Name,
	-- NULL Last_Month,
	-- NULL Last_Year,
	agg.Runrate,
	-- cast(avg(Payt) as decimal(38, 2)) Runrate_Coll,
	current_date Insert_Date
from (
	select
		filename,
		Financial_Account_ID,
		Payment_ID,
		CFU,
		CASE
			WHEN status in ('CAN', 'CAN-CREDIT', 'H') THEN 'Historic'
			ELSE 'Active'
		END Status,
		Entity,
		Source,
		Posting_Date,
		Payt,
		CASE
			WHEN CFU = 'SG' THEN trim(coalesce(gbu_name,''))
			WHEN CFU = 'EG' THEN trim(coalesce(gbu_name_final, ''))
			ELSE ''
		END as GBU_Name,
		CASE
			WHEN Posting_Date < current_date - interval '1' day  THEN 1
			ELSE 0
		END as Runrate
	from current_query
) as agg
GROUP BY agg.Financial_Account_ID, agg.CFU, agg.Status, agg.Entity, agg.Posting_Date, agg.Source, agg.GBU_Name, agg.Runrate 