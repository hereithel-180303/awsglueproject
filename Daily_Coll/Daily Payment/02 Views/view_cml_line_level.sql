CREATE OR REPLACE VIEW "view_cml_line_level" AS 
SELECT
  num
, group_id
, gbu_code
, gbu_segment
, salesforce_id
, account_id
, customer_id
, subscriber_id
, msisdn
, CAST(financial_account_id AS bigint) financial_account_id
, customer_alias
, final_account_name
, customer_sub_type_desc
, industry
, address_id
, company_address
, company_address_zip
, full_installation_address
, installation_zip
, source
, product_group
, product_type
, mpid
, main_plan_name
, main_plan_msf_vatex
, monthly_recurring_charge
, billed_rev_uc_oc
, otc_ots
, final_msf
, plan_mix
, bandwidth
, cabinet
, distribution_point_id
, subscriber_activation_date
, subscriber_tenure_months
, contract_start_date
, contract_end_date
, contract_status
, cml_type
, ict_product_category
, product_name
, charge_type
, otc_year
, am_assignment
, sales_cluster
, sales_region
, status_new_or_existing
, tier
, service_type
, technology
, service_group
, credit_limit
, outstanding_balance
, ob_bucket
, bill_cutoff
, segment
, lat
, long
, "fta_ada_ali (for the month)" fta_ada_ali_for_the_month
, dt
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "cml_line_level"
