CREATE OR REPLACE VIEW "view_bl_rcpts" AS 
SELECT
  CAST("cust_ac_no" AS bigint) "Financial_Account_ID"
, "concat"(cust_ac_no, "date_format"("date_parse"(posting_date, '%m/%d/%Y'), '%Y%m%d'), "replace"(CAST((CAST("replace"(converted, ',', '') AS double) * -1) AS varchar), '.', '')) "Payment_ID"
, "receipt_date"
, (CAST("replace"(receipt_amt, ',', '') AS double) * -1) receipt_amt
, "date_format"("date_parse"("replace"(posting_date, '  ', ' '), '%m/%d/%Y'), '%Y%m%d') "posting_date"
, "manual_or"
, "collector_id"
, "bank_sort_cd"
, "source"
, "ceris_no"
, "user_id"
, "ceris_batch_name"
, "receipt_method"
, "receipt_desc"
, "home_curr_amt"
, "curr_code"
, "amount"
, (CAST("replace"(converted, ',', '') AS double) * -1) converted
, "cust_classn"
, "ac_cat"
, "ac_stat"
, "company_id"
, "sequesnce_no"
, "cheque_no"
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "bl_rcpts"
