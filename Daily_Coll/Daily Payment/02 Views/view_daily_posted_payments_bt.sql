CREATE OR REPLACE VIEW "view_daily_posted_payments_bt" AS 
SELECT
  (CASE WHEN (actual_payment_currency = 'USD') THEN "concat"(cust_ac_no, "date_format"("date_parse"("replace"(post_date, '  ', ' '), '%b %d %Y %h:%i%p'), '%Y%m%d'), "replace"(CAST((CAST("replace"(receipt_amt, ',', '') AS double) * -1) AS varchar), '.', '')) ELSE "concat"(cust_ac_no, "date_format"("date_parse"("replace"(post_date, '  ', ' '), '%b %d %Y %h:%i%p'), '%Y%m%d'), "replace"(CAST((CAST("replace"(actual_amt_received, ',', '') AS double) * -1) AS varchar), '.', '')) END) Payment_ID
, CAST("cust_ac_no" AS bigint) "Financial_Account_ID"
, "cust_name" "cust_name"
, "ac_stat" "ac_stat"
, "bill_currency_code" "bill_currency_code"
, "amount" "amount"
, "application_period" "application_period"
, "actual_payment_currency" "actual_payment_currency"
, (CAST("replace"(actual_amt_received, ',', '') AS double) * -1) actual_amt_received
, (CAST("replace"(receipt_amt, ',', '') AS double) * -1) receipt_amt
, "receipt_dt" "receipt_dt"
, "date_format"("date_parse"("replace"(post_date, '  ', ' '), '%b %d %Y %h:%i%p'), '%Y%m%d') "post_date"
, "analyst_cd" "analyst_cd"
, "coll_cd" "coll_cd"
, "ref_no" "ref_no"
, "ceris_no" "ceris_no"
, "user_id" "user_id"
, "cheque_no" "cheque_no"
, "expiry_dt" "expiry_dt"
, "bank_sort_cd" "bank_sort_cd"
, "cheque_dt" "cheque_dt"
, "fran_area" "fran_area"
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
, dt
, entity
FROM
  "daily_posted_payments"
WHERE (Entity IN ('Bayan'))
