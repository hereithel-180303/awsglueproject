CREATE OR REPLACE VIEW "view_iccbs_lookup" AS 
SELECT
  acctg_grp
, classification
, cust_classn
, ac_cat
, ac_cat_desc
, acct_grp
, ao_cohort
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "iccbs_lookup"
