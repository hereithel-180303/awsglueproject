CREATE OR REPLACE VIEW "view_339__detail_payments_report___wln_i" AS 
SELECT
  CAST("account number" AS bigint) "Financial_Account_ID"
, "payment id" "Payment_ID"
, "primary msisdn" "primary_msisdn"
, "salutation" "salutation"
, "first name" "first_name"
, "middle name" "middle_name"
, "last name" "last_name"
, "corporate name" "corporate_name"
, "trim"("account type") "account_type"
, "trim"("account sub-type") "account_sub_type"
, "status" "status"
, "date_format"("date_parse"("posting date", '%b %d, %Y %h:%i:%s %p'), '%Y%m%d') "posting_date"
, "receipt date" "receipt_date"
, CAST("replace"("receipt amount", ',', '') AS double) receipt_amount
, "payment method" "payment_method"
, "corp id" "corp_id"
, "payment source" "payment_source"
, "payment source id" "payment_source_id"
, "reference or or number" "reference_or_or_number"
, "outstanding balance" "outstanding_balance"
, "cheque number" "cheque_number"
, "credit id" "credit_id"
, "payment reference id" "payment_reference_id"
, "sap store code" "sap_store_code"
, "account currency" "account_currency"
, "payment currency" "payment_currency"
, "amount in payment currency" "amount_in_payment_currency"
, "amount in peso" "amount_in_peso"
, "zero tag" "zero_tag"
, dt
, entity
, "regexp_extract"("$path", '[0-9.]+ [ \w-]+?(?=\.)') filename
FROM
  "detail_payments_report__339"
WHERE (entity = 'Innove')
