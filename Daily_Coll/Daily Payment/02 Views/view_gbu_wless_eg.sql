CREATE OR REPLACE VIEW "view_gbu_wless_eg" AS 
SELECT
  CAST(cust_ac_no AS bigint) financial_account_id
, company_name
, assignee_name
, postal_code
, balance
, overdue
, balance_90up
, cfwd_balance
, last_payment_date
, last_payment_amount
, bucket0___current
, bucket1___1_29_days
, bucket2___30_59_days
, bucket3___60_89_days
, bucket4___90_119_days
, bucket5___120_149_days
, bucket6___150_179_days
, bucket7___180_209_days
, bucket8___210_239_days
, bucket9___240_269_days
, bucket10___270__days
, last_payment_amount___original
, collection_status
, aging_grp
, sub_type_desc
, acctg_grp_new
, status_2
, account_type
, class
, company_code
, gbu_name_final
, cluster_final
, industry_final
, am_final
, analyst_final
, tier_final
, agency_final
, gbu_nsp_3mos_tag
, gbu_nsp_6mos_tag
, payt_last_3mos
, payt_last_6mos
, rc_202211
, payments_202301
, cust_id
, bill_summ_id
, bill_cutoff_date
, ar_age_202301
, billing_offer_name
, vendor
, entity_status
, days_past_due
, exception
, activation
, activation_month
, fa_mod
, conso_gbu
, mybss_tag
, dts_cnt
, dts_amt
, hotlist_tag
, loar
, dt
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "gbu_wless_eg"
