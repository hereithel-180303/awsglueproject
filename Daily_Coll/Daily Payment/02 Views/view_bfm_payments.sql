CREATE OR REPLACE VIEW "view_bfm_payments" AS 
SELECT
  "payment_id" "Payment_ID"
, CAST("account_to" AS bigint) "Financial_Account_ID"
, "payment_source_id" "payment_source_id"
, "account_from" "account_from"
, "l9_msisdn" "l9_msisdn"
, "amount_from" "amount_from"
, "original_amount" "original_amount"
, CAST("transferred_amount" AS decimal(20, 2)) "transferred_amount"
, "deposit_date" "deposit_date"
, "date_format"("date_parse"("replace"(activity_date, '/', ''), '%m%d%Y'), '%Y%m%d') "activity_date"
, "payment_method" "payment_method"
, "credit_id" "credit_id"
, "l9_or_id" "l9_or_id"
, "confirmation_no" "confirmation_no"
, "check_no" "check_no"
, "check_drawer_name" "check_drawer_name"
, "l9_file_name" "l9_file_name"
, "bank_code" "bank_code"
, dt
, concat(split_part("regexp_extract"("$path", '[0-9]+[ \w-]+?(?=\.)'), '_', 1), '_', substr(regexp_extract("$path", '(\d{4}\d{2}\d{2})'), 5, 4), substr(regexp_extract("$path", '(\d{4}\d{2}\d{2})'), 1, 2), substr(regexp_extract("$path", '(\d{4}\d{2}\d{2})'), 3, 2)) filename
FROM
  "bfm_payments"
