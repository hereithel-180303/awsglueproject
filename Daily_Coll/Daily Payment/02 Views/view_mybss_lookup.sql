CREATE OR REPLACE VIEW "view_mybss_lookup" AS 
SELECT
  report_grouping
, customer_type
, customer_sub_type
, customer_type_description
, customer_sub_type_description
, class
, new
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "mybss_lookup"
