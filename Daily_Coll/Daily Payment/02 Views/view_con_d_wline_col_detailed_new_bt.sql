CREATE OR REPLACE VIEW "view_con_d_wline_col_detailed_new_bt" AS 
SELECT
  CAST(cust_ac_no AS bigint) Financial_Account_ID
, "concat"(cust_ac_no, "date_format"("date_parse"(cashbox_dt, '%Y%m%d'), '%Y%m%d'), "replace"(CAST((CAST("replace"(receipt_amt, ',', '') AS double) * -1) AS varchar), '.', '')) Payment_ID
, receipt_dt
, "date_format"("date_parse"(cashbox_dt, '%Y%m%d'), '%Y%m%d') cashbox_dt
, (CAST("replace"(receipt_amt, ',', '') AS double) * -1) receipt_amt
, ac_cat
, cust_classn
, ac_stat
, ac_cat_desc
, cust_classn_desc
, fran_area
, dt
, entity
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "con_d_wline_col_detailed_new"
WHERE (entity IN ('Bayan'))
