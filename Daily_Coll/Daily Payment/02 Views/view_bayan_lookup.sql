CREATE OR REPLACE VIEW "view_bayan_lookup" AS 
SELECT
  CAST(cust_ac_no AS bigint) financial_account_id
, ac_stat
, cust_classn
, cust_classn_desc
, ac_cat
, ac_cat_desc
, last_name
, first_name
, fran_area
, os_balance
, credits
, olt30days
, o30to60days
, o60to90days
, o90to120days
, o120to150days
, o150to180days
, o180to210days
, ogt210days
, currency_cd
, source
, source_real
, age
, service
, acctg_grp_old
, classification_old
, acctg_grp
, classification
, inst_st_dt
, salesman_id
, postal_cd
, province
, bill_postal_cd
, tenure
, region
, prod_id
, prod_name
, svc_type
, package
, product
, tenure_group
, dt
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  "bayan_lookup"
