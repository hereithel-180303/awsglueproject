CREATE EXTERNAL TABLE `nsp_b`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `inst_st_dt` string COMMENT 'from deserializer', 
  `msf_new` string COMMENT 'from deserializer', 
  `inst_cohort` string COMMENT 'from deserializer', 
  `ac_stat` string COMMENT 'from deserializer', 
  `churn_dt` string COMMENT 'from deserializer', 
  `mob` string COMMENT 'from deserializer', 
  `cohort` string COMMENT 'from deserializer', 
  `payment` string COMMENT 'from deserializer', 
  `nsp_tag` string COMMENT 'from deserializer', 
  `inst_st_dt_new` string COMMENT 'from deserializer', 
  `closed_dt` string COMMENT 'from deserializer', 
  `age` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/consumer_bb/historical/nsp_b'
TBLPROPERTIES (
  'serialization.null.format'='', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1683081103')