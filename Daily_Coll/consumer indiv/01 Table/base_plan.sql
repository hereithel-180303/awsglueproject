CREATE EXTERNAL TABLE `base_plan`(
  `financial_account_id` string COMMENT 'from deserializer', 
  `billing_offer_name` string COMMENT 'from deserializer', 
  `fixed_amount` string COMMENT 'from deserializer', 
  `plan_msf` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/consumer_wls/historical/base_plan'
TBLPROPERTIES (
  'classification'='csv', 
  'serialization.null.format'='', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1684402043')