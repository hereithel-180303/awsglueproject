CREATE EXTERNAL TABLE `wless_tag`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `account_type` string COMMENT 'from deserializer', 
  `account_sub_type` string COMMENT 'from deserializer', 
  `financial_account_id` string COMMENT 'from deserializer', 
  `billing_offer_name` string COMMENT 'from deserializer', 
  `fixed_amount` string COMMENT 'from deserializer', 
  `plan_msf` string COMMENT 'from deserializer', 
  `base_plan_name` string COMMENT 'from deserializer', 
  `billing_offer_name_new` string COMMENT 'from deserializer', 
  `from_newact` string COMMENT 'from deserializer', 
  `shp_tag` string COMMENT 'from deserializer', 
  `nomadic_tag` string COMMENT 'from deserializer', 
  `nomadic_tag_old` string COMMENT 'from deserializer', 
  `segment` string COMMENT 'from deserializer', 
  `segment_final` string COMMENT 'from deserializer', 
  `plan_offer` string COMMENT 'from deserializer', 
  `plan_offer2` string COMMENT 'from deserializer', 
  `blank_tag` string COMMENT 'from deserializer', 
  `plan_msf_new` string COMMENT 'from deserializer', 
  `plan_offer_new` string COMMENT 'from deserializer', 
  `plan_offer2_new` string COMMENT 'from deserializer', 
  `plan_msf2` string COMMENT 'from deserializer', 
  `msf_tag` string COMMENT 'from deserializer', 
  `amount___total_msf_of_the_base_p` string COMMENT 'from deserializer', 
  `plan_msf2_new` string COMMENT 'from deserializer', 
  `msf_tag_new` string COMMENT 'from deserializer', 
  `segment_final_2` string COMMENT 'from deserializer', 
  `segment_final_new` string COMMENT 'from deserializer', 
  `msf_temp` string COMMENT 'from deserializer', 
  `msf_final` string COMMENT 'from deserializer', 
  `msf_range_final` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/consumer_wls/historical/wless_tag'
TBLPROPERTIES (
  'classification'='csv', 
  'serialization.null.format'='', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1684402730')