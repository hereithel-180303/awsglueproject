CREATE EXTERNAL TABLE `primary_msisdn_crm_active`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `account_status` string COMMENT 'from deserializer', 
  `open_date` string COMMENT 'from deserializer', 
  `msisdn` string COMMENT 'from deserializer', 
  `error` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/collections/reference/primary_msisdn_crm_active'
TBLPROPERTIES (
  'classification'='csv', 
  'serialization.null.format'='', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1684404289')