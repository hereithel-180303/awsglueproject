CREATE EXTERNAL TABLE `total_nomadic`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `base_plan_name` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/collections/reference/total_nomadic'
TBLPROPERTIES (
  'classification'='csv', 
  'serialization.null.format'='', 
  'skip.header.line.count'='1', 
  'transient_lastDdlTime'='1684402218')