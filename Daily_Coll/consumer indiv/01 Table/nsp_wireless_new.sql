CREATE EXTERNAL TABLE `nsp_wireless_new`(
  `cust_ac_no` string COMMENT 'from deserializer', 
  `payment` string COMMENT 'from deserializer', 
  `payment_net_handset` string COMMENT 'from deserializer', 
  `nsp_tag` string COMMENT 'from deserializer', 
  `cohort` string COMMENT 'from deserializer', 
  `customer_type` string COMMENT 'from deserializer', 
  `activation_dt` string COMMENT 'from deserializer', 
  `base_plan_name` string COMMENT 'from deserializer', 
  `plan_msf` string COMMENT 'from deserializer', 
  `plan_msf_no_zp` string COMMENT 'from deserializer', 
  `mob` string COMMENT 'from deserializer', 
  `status_2` string COMMENT 'from deserializer', 
  `closed_date` string COMMENT 'from deserializer', 
  `closed_date2` string COMMENT 'from deserializer', 
  `nsp_tag_old` string COMMENT 'from deserializer', 
  `payment_net_handset_new` string COMMENT 'from deserializer')
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'escapeChar'='\\', 
  'quoteChar'='\"', 
  'separatorChar'=',') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://s3cbrmddv02/source/collections/historical/nsp_wireless_new'
TBLPROPERTIES (
  'serialization.null.format'='', 
  'skip.header.line.count'='1')