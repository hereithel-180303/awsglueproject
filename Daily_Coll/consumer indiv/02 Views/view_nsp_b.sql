CREATE OR REPLACE VIEW "view_nsp_b" AS 
SELECT
  CUST_AC_NO
, INST_ST_DT
, msf_new
, inst_cohort
, AC_STAT
, churn_dt
, mob
, cohort
, payment
, nsp_tag
, inst_st_dt_new
, closed_dt
, AGE
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  nsp_b
