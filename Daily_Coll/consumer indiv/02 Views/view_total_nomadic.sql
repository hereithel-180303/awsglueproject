CREATE OR REPLACE VIEW "view_total_nomadic" AS 
SELECT
  CAST(cust_ac_no AS bigint) cust_ac_no
, Base_Plan_Name
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  total_nomadic
