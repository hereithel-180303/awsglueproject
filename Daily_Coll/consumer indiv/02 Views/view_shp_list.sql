CREATE OR REPLACE VIEW "view_shp_list" AS 
SELECT
  CAST(cust_ac_no AS bigint) cust_ac_no
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  shp_list
