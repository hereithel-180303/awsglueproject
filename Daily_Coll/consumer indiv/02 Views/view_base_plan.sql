CREATE OR REPLACE VIEW "view_base_plan" AS 
SELECT
  CAST(Financial_Account_ID AS bigint) Financial_Account_ID
, Billing_Offer_Name
, Fixed_Amount
, plan_msf
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  base_plan
