CREATE OR REPLACE VIEW "view_primary_msisdn_crm_active" AS 
SELECT
  CAST(CUST_AC_NO AS Bigint) CUST_AC_NO
, ACCOUNT_STATUS
, OPEN_DATE
, MSISDN
, ERROR
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  primary_msisdn_crm_active
