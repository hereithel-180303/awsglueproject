CREATE OR REPLACE VIEW "view_nsp_wireless_new" AS 
SELECT
  cust_ac_no
, payment
, payment_net_handset
, nsp_tag
, cohort
, Customer_Type
, activation_dt
, Base_Plan_Name
, plan_msf
, plan_msf_no_zp
, mob
, status_2
, closed_date
, closed_date2
, nsp_tag_old
, payment_net_handset_new
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  nsp_wireless_new
