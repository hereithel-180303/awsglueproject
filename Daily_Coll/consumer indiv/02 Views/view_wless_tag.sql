CREATE OR REPLACE VIEW "view_wless_tag" AS 
SELECT
  CAST(cust_ac_no AS bigint) cust_ac_no
, Account_Type
, Account_Sub_Type
, Financial_Account_ID
, Billing_Offer_Name
, Fixed_Amount
, plan_msf
, Base_Plan_Name
, billing_offer_name_new
, from_newact
, SHP_tag
, Nomadic_tag
, Nomadic_tag_old
, segment
, segment_final
, plan_offer
, plan_offer2
, blank_tag
, plan_msf_new
, plan_offer_new
, plan_offer2_new
, plan_msf2
, msf_tag
, Amount___total_MSF_of_the_base_p
, plan_msf2_new
, msf_tag_new
, segment_final_2
, segment_final_new
, msf_temp
, msf_final
, msf_range_final
, "regexp_extract"("$path", '[ \w-]+?(?=\.)') filename
FROM
  wless_tag
